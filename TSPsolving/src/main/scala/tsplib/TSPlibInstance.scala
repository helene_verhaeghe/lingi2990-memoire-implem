/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsplib

import scala.io.Source
import graph._

/**
 * Object containing all the informations
 * loaded from the TSPlib format files
 *
 * TSPlib : http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/
 */
class TSPlibInstance(
  val optimal: Int,
  val instance_name: String,
  val instance_type: String,
  val instance_comment: String,
  val instance_dimension: Int,
  val instance_edge_weight_type: String,
  val instance_edge_weight_format: String,
  val instance_matrix: Array[Array[Int]]) {

  /**
   * Create an instance of Abstract Graph from it
   */
  def toGraph: AbstractGraph = {
    val vertice = 0 until instance_dimension
    val edge = for {
      v1 <- vertice
      v2 <- vertice
      if v1 != v2
    } yield {
      (v1, v2, instance_matrix(v1)(v2))
    }
    val name = "TSPlib instance " + instance_name
    AbstractGraph(vertice, edge, name)
  }

  /**
   * Create a sub-instance of it by keeping only
   * the n first vertexes
   */
  def srink(name: String, n: Int, optimal: Int): TSPlibInstance = {
    val new_optimal = optimal
    val new_instance_name = name
    val new_instance_type = instance_type
    val new_instance_comment = instance_comment
    val new_instance_dimension = n
    val new_instance_edge_weight_type = instance_edge_weight_type
    val new_instance_edge_weight_format = instance_edge_weight_format
    val new_instance_matrix = instance_matrix.take(n).map(l => l.take(n))

    TSPlibInstance(new_optimal,
      new_instance_name,
      new_instance_type,
      new_instance_comment,
      new_instance_dimension,
      new_instance_edge_weight_type,
      new_instance_edge_weight_format,
      new_instance_matrix)
  }

  /**
   * Compute the mean of the cost of the graph
   */
  def meanOfCost: Double = {
    val vertice = 0 until instance_dimension
    var res = 0.0
    for {
      v1 <- vertice
      v2 <- vertice
      if v1 != v2
    } {
      res += instance_matrix(v1)(v2)
    }
    res / (instance_dimension * (instance_dimension - 1))
  }

  /**
   * Compute the minimum, maximum and mean
   * of the cost of the edges
   */
  def minmaxmeanCosts: (Int, Int, Double) = {
    val vertice = 0 until instance_dimension
    val costs = for {
      v1 <- vertice
      v2 <- vertice
      if v1 != v2
    } yield {
      instance_matrix(v1)(v2)
    }

    (costs.min, costs.max, costs.sum / (instance_dimension * (instance_dimension - 1.0)))
  }

  /**
   * Create a string of the graph as formated in the TSPlib files
   */
  override def toString: String = {
    "NAME: " + instance_name + "\n" +
      "TYPE: " + instance_type + "\n" +
      "COMMENT: " + instance_comment + "\n" +
      "DIMENSION: " + instance_dimension + "\n" +
      "EDGE_WEIGHT_TYPE: " + instance_edge_weight_type + "\n" +
      "EDGE_WEIGHT_FORMAT: " + instance_edge_weight_format + "\n" +
      "EDGE_WEIGHT_SECTION\n" +
      instance_matrix.map(_.mkString("\t")).mkString("\n") +
      "\nEOF"
  }

  /**
   * Create a one line string containing the following info
   * of the graph :
   * name, optimal, dimension, min cost, max cost, mean cost
   */
  def rowCharacteristic(separateur: String): String = {
    val (mincost, maxcost, meancost) = minmaxmeanCosts
    Array(instance_name, optimal, instance_dimension, mincost, maxcost, meancost).mkString(separateur)
  }

}
/**
 * Companion object of the class
 */
object TSPlibInstance {
  def apply(optimal: Int,
    instance_name: String,
    instance_type: String,
    instance_comment: String,
    instance_dimension: Int,
    instance_edge_weight_type: String,
    instance_edge_weight_format: String,
    instance_matrix: Array[Array[Int]]) = {
    new TSPlibInstance(optimal, instance_name, instance_type, instance_comment, instance_dimension, instance_edge_weight_type, instance_edge_weight_format, instance_matrix)
  }
}

