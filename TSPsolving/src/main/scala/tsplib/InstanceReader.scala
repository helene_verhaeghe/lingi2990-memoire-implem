/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsplib

import scala.io.Source

/**
 * Object to read an instance of the TSPlib
 * from the directory "input/ALL_atsp"
 *
 * TSPlib : http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/
 */
object ReaderInstance {
  def apply(name: String, optimal: Int): TSPlibInstance = {
    val lines = Source.fromFile("input/ALL_atsp/" + name + ".atsp").getLines
    var inMatrix = false
    var i = 0
    var j = 0
    var instance_name = ""
    var instance_type = ""
    var instance_comment = ""
    var instance_dimension = 0
    var instance_edge_weight_type = ""
    var instance_edge_weight_format = ""
    var instance_matrix: Array[Array[Int]] = Array()
    for (line <- lines) {
      var vals = line.split("[ ,\t]").toList.filter(p => (p != " ") && (p != "\t") && (p != ""))
      if (!vals.isEmpty) {
        if (!inMatrix) {
          vals.head match {
            case "NAME:" => instance_name = vals.tail.mkString(" ")
            case "TYPE:" => instance_type = vals.tail.mkString(" ")
            case "COMMENT:" => instance_comment = vals.tail.mkString(" ")
            case "DIMENSION:" => instance_dimension = vals(1).toInt
            case "EDGE_WEIGHT_TYPE:" => instance_edge_weight_type = vals.tail.mkString(" ")
            case "EDGE_WEIGHT_FORMAT:" => instance_edge_weight_format = vals.tail.mkString(" ")
            case "EDGE_WEIGHT_SECTION" => {
              inMatrix = true
              instance_matrix = Array.fill(instance_dimension)(Array.fill(instance_dimension)(0))
            }
            case "EOF" =>
            case other => println("other tag")
          }
        } else {
          for (c <- vals) {
            (instance_matrix(i))(j) = c.toInt
            j += 1
            if (j == instance_dimension) {
              j = 0
              i += 1
            }
          }
          if (i == instance_dimension)
            inMatrix = false
        }
      }
    }
    TSPlibInstance(optimal, instance_name, instance_type, instance_comment, instance_dimension, instance_edge_weight_type, instance_edge_weight_format, instance_matrix)
  }
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object TestInstanceReader extends App {
  val libinstance = ReaderInstance("br17", 39)
  println(libinstance)
  println(libinstance toGraph)
}