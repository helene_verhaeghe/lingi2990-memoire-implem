/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsplib

/**
 * Object loading all the instances of the TSPlib
 * from the directory "input/ALL_atsp"
 *
 * TSPlib : http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/
 */
object TSPlib {
  def br17 = { ReaderInstance("br17", 39) }
  def ft53 = { ReaderInstance("ft53", 6905) }
  def ft70 = { ReaderInstance("ft70", 38673) }
  def ftv33 = { ReaderInstance("ftv33", 1286) }
  def ftv35 = { ReaderInstance("ftv35", 1473) }
  def ftv38 = { ReaderInstance("ftv38", 1530) }
  def ftv44 = { ReaderInstance("ftv44", 1613) }
  def ftv47 = { ReaderInstance("ftv47", 1776) }
  def ftv55 = { ReaderInstance("ftv55", 1608) }
  def ftv64 = { ReaderInstance("ftv64", 1839) }
  def ftv70 = { ftv170.srink("ftv70", 71, 1950) }
  def ftv90 = { ftv170.srink("ftv90", 91, 1579) }
  def ftv100 = { ftv170.srink("ftv100", 101, 1788) }
  def ftv110 = { ftv170.srink("ftv110", 111, 1958) }
  def ftv120 = { ftv170.srink("ftv120", 121, 2166) }
  def ftv130 = { ftv170.srink("ftv130", 131, 2307) }
  def ftv140 = { ftv170.srink("ftv140", 141, 2420) }
  def ftv150 = { ftv170.srink("ftv150", 151, 2611) }
  def ftv160 = { ftv170.srink("ftv160", 161, 2683) }
  def ftv170 = { ReaderInstance("ftv170", 2755) }
  def kro124 = { ReaderInstance("kro124p", 36230) }
  def p43 = { ReaderInstance("p43", 5620) }
  def rgb323 = { ReaderInstance("rbg323", 1326) }
  def rgb358 = { ReaderInstance("rbg358", 1163) }
  def rgb403 = { ReaderInstance("rbg403", 2465) }
  def rgb443 = { ReaderInstance("rbg443", 2720) }
  def ry48p = { ReaderInstance("ry48p", 14422) }
  def ALL_TSPlib = Array(
    br17,
    ft53,
    ft70,
    ftv33,
    ftv35,
    ftv38,
    ftv44,
    ftv47,
    ftv55,
    ftv64,
    ftv70,
    ftv90,
    ftv100,
    ftv110,
    ftv120,
    ftv130,
    ftv140,
    ftv150,
    ftv160,
    ftv170,
    kro124,
    p43,
    rgb323,
    rgb358,
    rgb403,
    rgb443,
    ry48p)
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object TestTSPlib extends App {
  for (graphfromlib <- TSPlib.ALL_TSPlib) {
    println(graphfromlib.rowCharacteristic("\t& ") + " \\\\")
  }

}