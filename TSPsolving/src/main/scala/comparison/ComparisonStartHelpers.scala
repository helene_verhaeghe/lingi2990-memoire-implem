/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import myVisu._

/**
 * Specifics graphs
 */
trait SummaringGraph {
  val nameBench: String
  val nameGraph: String
  val rg: ResultGatherer
  var graph: ResultGraph = null

  val rangeNbNode = rg.range
  def defgraph: Unit
  def create: SummaringGraph
}
trait SummaringGraphByNode extends SummaringGraph {
  def defgraph: Unit = {
    graph = ResultGraph(nameGraph, "number of nodes", "ratio", rangeNbNode.min, rangeNbNode.max)
  }
  def addForNode(nbnode: Int): Unit
  def create: SummaringGraphByNode = {
    graph.create
    this
  }
}
trait SummaringGraphPP extends SummaringGraph {
  val rangeTau: Range
  def defgraph: Unit = {
    graph = ResultGraph(nameGraph, "tau", "% of graph where ratio > tau", rangeTau.min, rangeTau.max)
  }
  def add: Unit
  def create: SummaringGraphPP = {
    graph.create
    this
  }
}

case class PP(
  val nameBench: String,
  val stepType: String,
  val methods: Array[String],
  val sample: Int = 100,
  val inverted: Boolean = false,
  val rg: ResultGatherer = ResultGatherer(0 to 0)) extends SummaringGraphPP {

  val rangeTau = 0 to 6
  val nameGraph = "PP profile of the time of bench " + nameBench + " for method " + methods.mkString(",") + "with step " + stepType

  import scala.collection.mutable.Set
  import scala.util.Random
  val dataSet: Set[Array[Double]] = Set()
  defgraph

  val line_horizontal = graph.getHorizontalLine("100\\%", 1.0)
  val line = methods.map(s => graph.getPerformanceProfileLine(s))

  def addData(data: Array[Double]) = {
    if (inverted) {
      val maxData = data.max
      dataSet += data.map(x => 1.0 * x / maxData)
    } else {
      val minData = data.min
      dataSet += data.map(x => 1.0 * x / minData)
    }
  }

  def add: Unit = {
    for (i <- 0 until line.size) {
      line(i).addPoint(0, 0)
      val points = if (inverted) {
        dataSet.toArray.sortBy(x => -x(i)).zipWithIndex.map(p => (p._1(i), 1.0 * (p._2 + 1) / dataSet.size)).reverse
      } else {
        dataSet.toArray.sortBy(x => x(i)).zipWithIndex.map(p => (p._1(i), 1.0 * (p._2 + 1) / dataSet.size))
      }
      val k = 0
      for (
        p <- points;
        if (k <= 20 || k >= points.size - 20 || Random.nextInt(100) <= sample)
      ) {
        line(i).addPoint(p._1, p._2)
      }
    }
  }
}

/**
 * Graph representing the ratio of the number of graph
 * of the benchmark (classed by number of node) where the
 * arbo behave better for the statistical value
 */
case class RatioGraphStatistics(
  val nameBench: String,
  val rg: ResultGatherer, first: Symbol = 'a, second: Symbol = 't, isStrict: Boolean = false) extends SummaringGraphByNode {

  val nameGraph = "Pourcentage of the graphs where " + SwitchString.full(first) + " behave" + (if (isStrict) " strictly " else " ") + "better than " + SwitchString.full(second) + " the " + nameBench + " benchmark"

  defgraph

  val line_horizontal = graph.getHorizontalLine("100\\%", 1.0)
  val line_mean = graph.getEvolutiveLine("mean")
  val line_max = graph.getEvolutiveLine("maximum")
  val line_min = graph.getEvolutiveLine("minimum")
  val line_var = graph.getEvolutiveLine("variance")

  def addForNode(nbnode: Int) = {
    line_mean.addPoint(nbnode, rg.countRatioBetween(nbnode, first, second, 'mean, isStrict))
    line_max.addPoint(nbnode, rg.countRatioBetween(nbnode, first, second, 'max, isStrict))
    line_min.addPoint(nbnode, rg.countRatioBetween(nbnode, first, second, 'min, isStrict))
    line_var.addPoint(nbnode, rg.countRatioBetween(nbnode, first, second, 'var, isStrict))
  }
}

/**
 * Graph representing the ratio of the number of graph
 * of the benchmark (classed by number of node) where the
 * minimum of the arbo is better than the maximum of the tree
 */
case class RatioGraphMinMax(
  val nameBench: String,
  val rg: ResultGatherer, first: Symbol = 'a, second: Symbol = 't) extends SummaringGraphByNode {

  val nameGraph = "Pourcentage of the graphs where the 1-arborescence behave better than the 1-tree the " + nameBench + " benchmark"

  defgraph

  val line_horizontal = graph.getHorizontalLine("100\\%", 1.0)
  val line_minmax = graph.getEvolutiveLine("minimum of 1-arbo compared to maximum of 1-tree")
  val line_minmaxStrict = graph.getEvolutiveLine("minimum of 1-arbo compared to maximum of 1-tree")

  def addForNode(nbnode: Int) = {
    line_minmax.addPoint(nbnode, rg.countRatioMinMax(nbnode, first, second, false))
    line_minmaxStrict.addPoint(nbnode, rg.countRatioMinMax(nbnode, first, second, true))
  }
}

/**
 * For all the node of all the graphs, compute the ratio
 * 1arbo/max1tree, 1arbo/1treeIn, 1arbo/1treeOut
 * the line represent the % of graphs with ratio > tau
 * For arbo be best, must be at the right of tau=1
 * the most at right the best is
 */
case class PerfProfileGraphGlobalForEachNodeArboTree(
  val nameBench: String,
  val rg: ResultGatherer) extends SummaringGraphPP {
  val rangeTau: Range = 0 to 3
  val nameGraph: String = "Performance Profile graph on the " + nameBench + " benchmark concerning ratio computed for each nodes"

  defgraph

  val line_vertical = graph.getVerticalLine("tau = 1", 1.0, 0.0, 1.0)
  val line_global = graph.getPerformanceProfileLine("r = 1arbo/max1tree")
  val line_in = graph.getPerformanceProfileLine("r = 1arbo/1treeIn")
  val line_out = graph.getPerformanceProfileLine("r = 1arbo/1treeOut")

  def add: Unit = {
    val byNode = rg.countPPRatioArboTreeByNode
    line_global.addPoints(byNode._1)
    line_in.addPoints(byNode._2)
    line_out.addPoints(byNode._3)
  }
}

case class PerfProfileGraphByNodeArboTree(
  val nameBench: String,
  val rg: ResultGatherer) extends SummaringGraphPP {

  val nameGraph: String = "Performance Profile graph on the " + nameBench + " benchmark concerning ratio max1arbo/max1tree"
  val rangeTau: Range = 0 to 3

  defgraph

  val line_vertical = graph.getVerticalLine("tau = 1", 1.0, 0.0, 1.0)
  val line_all = rangeNbNode.toArray.map { i =>
    (i, graph.getPerformanceProfileLine(i + " nodes"))
  }

  def add: Unit = {
    for ((nbnode, line) <- line_all) {
      line.addPoints(rg.countPPRatioBetween(nbnode, 'a, 't, 'max))
    }
  }
}

case class PerfProfileGraphByGraphEachNode(
  val nameBench: String,
  val rg: ResultGatherer, first: Symbol = 'a, second: Symbol = 't) extends SummaringGraphPP {

  val nameGraph: String = "Performance Profile graph on the " + nameBench + " benchmark concerning ratio " + SwitchString.full(first) + "/" + SwitchString.full(second)
  val rangeTau: Range = 0 to 5

  defgraph

  val line_vertical = graph.getVerticalLine("tau = 1", 1.0, 0.0, 1.0)
  val line = graph.getPerformanceProfileLine("..")

  def add: Unit = {
    line.addPoints(rg.countPPRatioBetween(first, second, 'max))
  }
}
