/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import graph._
import branchAndBoundTSP.TSPBranchBound
import scala.collection.mutable.HashMap
import scala.collection.mutable.Set
import myVisu._
import java.io.PrintWriter
import scala.util.Random

/**
 * Compare the 1Tree's, 1Arbo's and 1Anti's for RandB and C
 */
object ComparisonStartRandB extends App {

  val randB = RandomSeed("RandB", 20 to 50, 100 to 100, 100 to 1000 by 100, 20)
  val randBbis = RandomSeed("RandB", 20 to 30, 100 to 100, 100 to 1000 by 100, 20)

  ComparisonStartRandomFull(randB, (s, i1, i2, i3) => GraphGenerator.gen(s, i1, i2, i3))

}
object ComparisonStartRandC extends App {

  val randC = RandomSeed("RandC", 20 to 50, 100 to 100, 100 to 1000 by 100, 20)
  val randCbis = RandomSeed("RandC", 20 to 30, 100 to 100, 100 to 1000 by 100, 20)

  ComparisonStartRandomFull(randC, (s, i1, i2, i3) => GraphGenerator.genBis(s, i1, i2, i3))

}

object ComparisonStartRandomFull {
  def apply(randomseed: RandomSeed, genmeth: (String, Int, Int, Int) => AbstractGraph) = {
    implicit val threshold = 0.00001
    val nameBench = randomseed.name
    val myprinter = PrintPointsRatioVar(nameBench)
    //  val myprinterD = PrintPointsRatioDegree(nameBench)

    val IDTEST = System.currentTimeMillis() + "_start"
    val rg = ResultGatherer(randomseed.sizeRange)

    // ===============
    // graph creation

    val graph1_ArboVsTree_1_better = RatioGraphStatistics(nameBench, rg, 'a, 't, false).create
    val graph1_ArboVsTree_2_strict = RatioGraphStatistics(nameBench, rg, 'a, 't, true).create
    val graph1_ArboVsTree_3_minmax = RatioGraphMinMax(nameBench, rg, 'a, 't).create
    val graph2_AntiVsTree_1_better = RatioGraphStatistics(nameBench, rg, 'aa, 't, false).create
    val graph2_AntiVsTree_2_strict = RatioGraphStatistics(nameBench, rg, 'aa, 't, true).create
    val graph2_AntiVsTree_3_minmax = RatioGraphMinMax(nameBench, rg, 'aa, 't).create
    val graph3_ArboVsAnti_1_better = RatioGraphStatistics(nameBench, rg, 'a, 'aa, false).create
    val graph3_ArboVsAnti_2_strict = RatioGraphStatistics(nameBench, rg, 'a, 'aa, true).create
    val graph3_ArboVsAnti_3_minmax = RatioGraphMinMax(nameBench, rg, 'a, 'aa).create
    val graph3_ArboVsAnti_4_minmax = RatioGraphMinMax(nameBench, rg, 'aa, 'a).create

    val graph4_ArboVsTree_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'a, 't).create
    val graph4_AntiVsTree_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'aa, 't).create
    val graph4_ArboVsAnti_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'a, 'aa).create

    val graph_pp = PP(nameBench, "", Array("1Arbo", "1Tree"), 30)
    graph_pp.create
    val graph_pp_only = PP(nameBench, "", Array("1Arbo", "1Tree"), 30)
    graph_pp_only.create
    // ===============

    var id = 0

    for (nbnode <- randomseed.sizeRange) {
      for {
        pourcent <- randomseed.pourcentRange;
        maxcost <- randomseed.costRange;
        nbgraph <- 1 to randomseed.nbgraph
      } {
        id += 1
        val idTag = "" + IDTEST + "-" + id
        val graph = genmeth(idTag, nbnode, maxcost, pourcent)

        val time: Array[Long] = Array.fill(4)(0)

        TimeSaver.setOffset
        val a = graph.toEdmondsGraph
        time(0) = TimeSaver.timePassed
        TimeSaver.setOffset
        val a2 = a.all1arbo
        time(1) = TimeSaver.timePassed

        TimeSaver.setOffset
        val c = graph.getSymmetricVersion.toHKgraph
        time(2) = TimeSaver.timePassed
        TimeSaver.setOffset
        val c2 = c.all1Tree(true)
        time(3) = TimeSaver.timePassed

        graph_pp.addData(Array(time(0) + 1.0 * time(1) / nbnode, time(2) + 0.5 * time(3) / nbnode))
        graph_pp_only.addData(Array(1.0 * time(1) / nbnode, 0.5 * time(3) / nbnode))

        val result = ResultIndividualGraph(nbnode, 0.0, //  <==
          a2,
          graph.toEdmondsGraph.all1antiarbo,
          c2)
        rg.add(result)

        myprinter.feed(graph, result)

      }

      graph1_ArboVsTree_1_better.addForNode(nbnode)
      graph1_ArboVsTree_2_strict.addForNode(nbnode)
      graph1_ArboVsTree_3_minmax.addForNode(nbnode)
      graph2_AntiVsTree_1_better.addForNode(nbnode)
      graph2_AntiVsTree_2_strict.addForNode(nbnode)
      graph2_AntiVsTree_3_minmax.addForNode(nbnode)
      graph3_ArboVsAnti_1_better.addForNode(nbnode)
      graph3_ArboVsAnti_2_strict.addForNode(nbnode)
      graph3_ArboVsAnti_3_minmax.addForNode(nbnode)
      graph3_ArboVsAnti_4_minmax.addForNode(nbnode)
    }

    graph4_ArboVsTree_pp.add
    graph4_AntiVsTree_pp.add
    graph4_ArboVsAnti_pp.add
    graph_pp.add
    graph_pp_only.add

    // ===============
    // graph print
    import printer._
    TikzGraphicPrinter(nameBench + "-graph1-ArboVsTree-1-better", graph1_ArboVsTree_1_better.graph)
    TikzGraphicPrinter(nameBench + "-graph1-ArboVsTree-2-strict", graph1_ArboVsTree_2_strict.graph)
    TikzGraphicPrinter(nameBench + "-graph1-ArboVsTree-3-minmax", graph1_ArboVsTree_3_minmax.graph)
    TikzGraphicPrinter(nameBench + "-graph2-AntiVsTree-1-better", graph2_AntiVsTree_1_better.graph)
    TikzGraphicPrinter(nameBench + "-graph2-AntiVsTree-2-strict", graph2_AntiVsTree_2_strict.graph)
    TikzGraphicPrinter(nameBench + "-graph2-AntiVsTree-3-minmax", graph2_AntiVsTree_3_minmax.graph)
    TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-1-better", graph3_ArboVsAnti_1_better.graph)
    TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-2-strict", graph3_ArboVsAnti_2_strict.graph)
    TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-3-minmax", graph3_ArboVsAnti_3_minmax.graph)
    TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-4-minmax", graph3_ArboVsAnti_4_minmax.graph)
    TikzGraphicPrinter(nameBench + "-graph4-ArboVsTree-pp", graph4_ArboVsTree_pp.graph)
    TikzGraphicPrinter(nameBench + "-graph4-AntiVsTree-pp", graph4_AntiVsTree_pp.graph)
    TikzGraphicPrinter(nameBench + "-graph4-ArboVsAnti-pp", graph4_ArboVsAnti_pp.graph)
    TikzGraphicPrinter(nameBench + "-graph5-pptime-full", graph_pp.graph)
    TikzGraphicPrinter(nameBench + "-graph5-pptime-only", graph_pp_only.graph)
    // ===============

    myprinter.close
    //  myprinterD.close

  }
}
