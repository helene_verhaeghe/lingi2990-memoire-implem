/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import graph._
import branchAndBoundTSP.TSPBranchBound
import scala.collection.mutable.HashMap
import scala.collection.mutable.Set
import myVisu._
import java.io.PrintWriter
import scala.util.Random

/**
 * Compare the 1Tree's, 1Arbo's and 1Anti's for RandA
 */
object ComparisonStart extends App {

  val randA = RandomSeed("RandA", 3 to 17, 10 to 100 by 10, 20 to 100 by 20, 20)
  val randAbis = RandomSeed("RandA", 3 to 10, 10 to 100 by 10, 20 to 100 by 20, 20)

  val randomseed = randA

  implicit val threshold = 0.00001
  val nameBench = randomseed.name
  val myprinter = PrintPointsRatioVar(nameBench)
  val myprinterD = PrintPointsRatioDegree(nameBench)

  val IDTEST = System.currentTimeMillis() + "_start"
  val rg = ResultGatherer(randomseed.sizeRange)

  // ===============
  // graph creation

  val graph1_ArboVsTree_1_better = RatioGraphStatistics(nameBench, rg, 'a, 't, false).create
  val graph1_ArboVsTree_2_strict = RatioGraphStatistics(nameBench, rg, 'a, 't, true).create
  val graph1_ArboVsTree_3_minmax = RatioGraphMinMax(nameBench, rg, 'a, 't).create
  val graph2_AntiVsTree_1_better = RatioGraphStatistics(nameBench, rg, 'aa, 't, false).create
  val graph2_AntiVsTree_2_strict = RatioGraphStatistics(nameBench, rg, 'aa, 't, true).create
  val graph2_AntiVsTree_3_minmax = RatioGraphMinMax(nameBench, rg, 'aa, 't).create
  val graph3_ArboVsAnti_1_better = RatioGraphStatistics(nameBench, rg, 'a, 'aa, false).create
  val graph3_ArboVsAnti_2_strict = RatioGraphStatistics(nameBench, rg, 'a, 'aa, true).create
  val graph3_ArboVsAnti_3_minmax = RatioGraphMinMax(nameBench, rg, 'a, 'aa).create
  val graph3_ArboVsAnti_4_minmax = RatioGraphMinMax(nameBench, rg, 'aa, 'a).create

  val graph4_ArboVsTree_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'a, 't).create
  val graph4_AntiVsTree_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'aa, 't).create
  val graph4_ArboVsAnti_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'a, 'aa).create

  // ===============

  var id = 0

  for (nbnode <- randomseed.sizeRange) {
    for {
      pourcent <- randomseed.pourcentRange;
      maxcost <- randomseed.costRange;
      nbgraph <- 1 to randomseed.nbgraph
    } {
      id += 1
      val idTag = "" + IDTEST + "-" + id
      val graph = GraphGenerator.gen(idTag, nbnode, maxcost, pourcent)
      val resBB = TSPBranchBound(graph)
      if (!resBB.value.isEmpty) {
        val result = ResultIndividualGraph(nbnode, resBB.value.get,
          graph.toEdmondsGraph.all1arbo,
          graph.getInvertedVersion.toEdmondsGraph.all1arbo,
          graph.getSymmetricVersion(-1000).toHKgraph.all1Tree(true))
        rg.add(result)

        myprinter.feed(graph, result)
        myprinterD.feed(graph, result)
      }

    }

    graph1_ArboVsTree_1_better.addForNode(nbnode)
    graph1_ArboVsTree_2_strict.addForNode(nbnode)
    graph1_ArboVsTree_3_minmax.addForNode(nbnode)
    graph2_AntiVsTree_1_better.addForNode(nbnode)
    graph2_AntiVsTree_2_strict.addForNode(nbnode)
    graph2_AntiVsTree_3_minmax.addForNode(nbnode)
    graph3_ArboVsAnti_1_better.addForNode(nbnode)
    graph3_ArboVsAnti_2_strict.addForNode(nbnode)
    graph3_ArboVsAnti_3_minmax.addForNode(nbnode)
    graph3_ArboVsAnti_4_minmax.addForNode(nbnode)

  }
  graph4_ArboVsTree_pp.add
  graph4_AntiVsTree_pp.add
  graph4_ArboVsAnti_pp.add

  // ===============
  // graph print
  import printer._
  TikzGraphicPrinter(nameBench + "-graph1-ArboVsTree-1-better", graph1_ArboVsTree_1_better.graph)
  TikzGraphicPrinter(nameBench + "-graph1-ArboVsTree-2-strict", graph1_ArboVsTree_2_strict.graph)
  TikzGraphicPrinter(nameBench + "-graph1-ArboVsTree-3-minmax", graph1_ArboVsTree_3_minmax.graph)
  TikzGraphicPrinter(nameBench + "-graph2-AntiVsTree-1-better", graph2_AntiVsTree_1_better.graph)
  TikzGraphicPrinter(nameBench + "-graph2-AntiVsTree-2-strict", graph2_AntiVsTree_2_strict.graph)
  TikzGraphicPrinter(nameBench + "-graph2-AntiVsTree-3-minmax", graph2_AntiVsTree_3_minmax.graph)
  TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-1-better", graph3_ArboVsAnti_1_better.graph)
  TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-2-strict", graph3_ArboVsAnti_2_strict.graph)
  TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-3-minmax", graph3_ArboVsAnti_3_minmax.graph)
  TikzGraphicPrinter(nameBench + "-graph3-ArboVsAnti-4-minmax", graph3_ArboVsAnti_4_minmax.graph)
  TikzGraphicPrinter(nameBench + "-graph4-ArboVsTree-pp", graph4_ArboVsTree_pp.graph)
  TikzGraphicPrinter(nameBench + "-graph4-AntiVsTree-pp", graph4_AntiVsTree_pp.graph)
  TikzGraphicPrinter(nameBench + "-graph4-ArboVsAnti-pp", graph4_ArboVsAnti_pp.graph)

  // ===============

  myprinter.close
  myprinterD.close
}

/**
 * Helping tools
 */
object SwitchString {
  def apply(s: Symbol): Int = {
    s match {
      case 'a => 0 // arborescence
      case 'aa => 1
      case 't => 2 // tree
      case 'ti => 3 // tree in
      case 'to => 4 // tree out node
    }
  }
  def full(s: Symbol): String = {
    s match {
      case 'a => "1Arbo"
      case 'aa => "1Anti"
      case 't => "1Tree"
      case 'ti => "1TreeIn"
      case 'to => "1TreeOut"
    }
  }
  def apply(s1: Symbol, s2: Symbol, stat: Symbol, strict: Boolean): ResultIndividualGraph => Boolean = {
    val s1Int = SwitchString(s1)
    val s2Int = SwitchString(s2)
    stat match {
      case 'max => if (strict) ((res: ResultIndividualGraph) => res.maxSC(s1Int)(s2Int)) else ((res: ResultIndividualGraph) => res.maxC(s1Int)(s2Int))
      case 'min => if (strict) ((res: ResultIndividualGraph) => res.minSC(s1Int)(s2Int)) else ((res: ResultIndividualGraph) => res.minC(s1Int)(s2Int))
      case 'mean => if (strict) ((res: ResultIndividualGraph) => res.meanSC(s1Int)(s2Int)) else ((res: ResultIndividualGraph) => res.meanC(s1Int)(s2Int))
      case 'var => if (strict) ((res: ResultIndividualGraph) => res.varSC(s1Int)(s2Int)) else ((res: ResultIndividualGraph) => res.varC(s1Int)(s2Int))
    }
  }
  def apply(s1: Symbol, s2: Symbol, stat: Symbol): ResultIndividualGraph => Double = {
    val s1Int = SwitchString(s1)
    val s2Int = SwitchString(s2)
    stat match {
      case 'max => ((res: ResultIndividualGraph) => res.maxR(s1Int)(s2Int))
      case 'min => ((res: ResultIndividualGraph) => res.minR(s1Int)(s2Int))
      case 'mean => ((res: ResultIndividualGraph) => res.meanR(s1Int)(s2Int))
      case 'var => ((res: ResultIndividualGraph) => res.varR(s1Int)(s2Int))
    }
  }
}

case class GroupData(arbo: List[Double], anti: List[Double], tree: List[Double], treeIn: List[Double], treeOut: List[Double]) {
  def result(fct: (List[Double], Int) => Double, inverted: Boolean = false): (Array[Double], Array[Array[Boolean]], Array[Array[Boolean]], Array[Array[Double]]) = {
    val values = Array(fct(arbo, 0), fct(anti, 1), fct(tree, 2), fct(treeIn, 3), fct(treeOut, 4))
    (values,
      values.map(a => values.map(b => if (!inverted) a >= b else a <= b)),
      values.map(a => values.map(b => if (!inverted) a > b else a < b)),
      values.map(a => values.map(b => a / b)))
  }
  override def toString = {
    "[\n" +
      arbo + "\n" +
      anti + "\n" +
      tree + "\n" +
      treeIn + "\n" +
      treeOut + "\n" +
      "]"
  }
}
case class ResultIndividualGraph(val nNode: Int, val optimal: Double, val arboInit: List[(Int, Double)], val antiInit: List[(Int, Double)], val treeInit: List[(Int, Double)]) {
  val treeInInitMap = treeInit.filter(p => p._1 >= nNode).map(p => (p._1 - nNode, p._2)).toMap
  val treeOutInitMap = treeInit.filter(p => p._1 < nNode).toMap
  val arboInitMap = arboInit.toMap

  // classed : 0:arbo, 1:anti, 2:tree, 3:treeIn, 4:treeOut
  val group = GroupData(arboInit.map(_._2),
    antiInit.map(_._2),
    treeInit.map(_._2),
    treeInit.filter(p => p._1 >= nNode).map(_._2),
    treeInit.filter(p => p._1 < nNode).map(_._2))

  val (maxV, maxC, maxSC, maxR) = group.result((l, i) => l.max)
  val (minV, minC, minSC, minR) = group.result((l, i) => l.min)
  val (meanV, meanC, meanSC, meanR) = group.result((l, i) => l.sum / l.length)
  val (varV, varC, varSC, varR) = group.result((l, i) => l.map(x => (x - meanV(i)) * (x - meanV(i))).sum / l.length, true)

  def minMax(a: Symbol, b: Symbol, strict: Boolean): Boolean = {
    if (strict)
      minV(SwitchString(a)) > maxV(SwitchString(b))
    else
      minV(SwitchString(a)) >= maxV(SwitchString(b))
  }

  val ratioByNode = { //tau = val 1 arbo / val 1 tree => (1arbo/1tree(max),1arbo/1treeIn,1arbo/1treeOut)
    Array.tabulate(nNode) { i =>
      val treeInVal = treeInInitMap(i)
      val treeOutVal = treeOutInitMap(i)
      val arboVal = arboInitMap(i)
      (arboVal / Math.max(treeInVal, treeOutVal), arboVal / treeInVal, arboVal / treeOutVal)
    }
  }

  override def toString: String = {
    group +
      "optimal : " + optimal + "\n" +
      "1arbo : \n" +
      "\tMax : " + maxV(0) + "\n" +
      "\tMin : " + minV(0) + "\n" +
      "\tMean: " + meanV(0) + "\n" +
      "\tVar : " + varV(0) + "\n" +
      "1anti : \n" +
      "\tMax : " + maxV(1) + "\n" +
      "\tMin : " + minV(1) + "\n" +
      "\tMean: " + meanV(1) + "\n" +
      "\tVar : " + varV(1) + "\n" +
      "1tree : \n" +
      "\tMax : " + maxV(2) + "\n" +
      "\tMin : " + minV(2) + "\n" +
      "\tMean: " + meanV(2) + "\n" +
      "\tVar : " + varV(2) + "\n" +
      "1treeIn : \n" +
      "\tMax : " + maxV(3) + "\n" +
      "\tMin : " + minV(3) + "\n" +
      "\tMean: " + meanV(3) + "\n" +
      "\tVar : " + varV(3) + "\n" +
      "1treeOut : \n" +
      "\tMax : " + maxV(4) + "\n" +
      "\tMin : " + minV(4) + "\n" +
      "\tMean: " + meanV(4) + "\n" +
      "\tVar : " + varV(4) + "\n"
  }

}

case class ResultGatherer(val range: Range) {
  val map: HashMap[Int, Set[ResultIndividualGraph]] = {
    val maptemp: HashMap[Int, Set[ResultIndividualGraph]] = HashMap()
    for (i <- range) { maptemp += ((i, Set())) }
    maptemp
  }

  def add(result: ResultIndividualGraph) = {
    map(result.nNode) += result
  }

  def countRatio(n: Int, test: ResultIndividualGraph => Boolean): Double = { 1.0 * map(n).count(test(_)) / map(n).size }
  def countRatioMinMax(n: Int, a: Symbol, b: Symbol, strict: Boolean = false): Double = countRatio(n, _.minMax(a, b, strict))
  def countRatioBetween(n: Int, a: Symbol, b: Symbol, stat: Symbol, strict: Boolean = false): Double = {
    val fct = SwitchString(a, b, stat, strict)
    countRatio(n, fct)
  }

  def countRatioAll(a: Symbol, b: Symbol, stat: Symbol, strict: Boolean = false) = {
    var number = 0
    var count = 0.0
    for (n <- range; if (map(n).size != 0)) {
      val ratio = countRatioBetween(n, a, b, stat, strict)
      number += map(n).size
      count += ratio * map(n).size
    }
    count / number
  }
  def countRatioAllMinMax(a: Symbol, b: Symbol, strict: Boolean = false) = {
    var number = 0
    var count = 0.0
    for (n <- range; if (map(n).size != 0)) {
      val ratio = countRatioMinMax(n, a, b, strict)
      number += map(n).size
      count += ratio * map(n).size
    }
    count / number
  }

  def countPPRatioAll(fct: ResultIndividualGraph => Double) = {
    val number = map.map(_._2.size).sum
    map.flatMap(s => s._2.toArray.map(e => fct(e))).toArray.sortBy(x => -x).zipWithIndex.map(x => (x._1, 1.0 * (x._2 + 1) / number)).reverse
  }
  def countPPRatioBetween(a: Symbol, b: Symbol, stat: Symbol) = {
    val fct = SwitchString(a, b, stat)
    countPPRatioAll(fct)
  }

  def countPPRatio(n: Int, fct: ResultIndividualGraph => Double) = {
    val number = map(n).size
    map(n).toArray.map(e => fct(e)).sortBy(x => -x).zipWithIndex.map(x => (x._1, 1.0 * (x._2 + 1) / number)).reverse
  }
  def countPPRatioBetween(n: Int, a: Symbol, b: Symbol, stat: Symbol) = {
    val fct = SwitchString(a, b, stat)
    countPPRatio(n, fct)
  }

  def countPPRatioArboTreeByNode = {
    val allValues = map.flatMap(s => s._2.toArray.flatMap(_.ratioByNode)).toArray
    val number = allValues.size
    val arboTreeValues = allValues.map(_._1).sortBy(x => -x).zipWithIndex.map(x => (x._1, 1.0 * (x._2 + 1) / number)).reverse
    val arboTreeInValues = allValues.map(_._2).sortBy(x => -x).zipWithIndex.map(x => (x._1, 1.0 * (x._2 + 1) / number)).reverse
    val arboTreeOutValues = allValues.map(_._3).sortBy(x => -x).zipWithIndex.map(x => (x._1, 1.0 * (x._2 + 1) / number)).reverse
    (arboTreeValues, arboTreeInValues, arboTreeOutValues)
  }
}

case class PrintPointsRatioVar(val nameBench: String) {

  val myprinter = Array(new PrintWriter("output/tikz/corrVar-" + nameBench + "-Max"), new PrintWriter("output/tikz/corrVar-" + nameBench + "-Mean"))

  def feed(graph: AbstractGraph, result: ResultIndividualGraph): Unit = {
    val ratio = graph.ratios
    val ratioMean = result.meanV(SwitchString('aa)) / result.meanV(SwitchString('a))
    val ratioMax = result.maxV(SwitchString('aa)) / result.maxV(SwitchString('a))
    print(ratio(3), ratioMean, ratioMax,
      ratioMax >= 20 ||
        ratioMean >= 20 ||
        ratio(3) >= 20 ||
        Random.nextInt(100) <= 80)
  }

  def feedAll(graph: AbstractGraph, result: ResultIndividualGraph): Unit = {
    val ratio = graph.ratios
    val ratioMean = result.meanV(SwitchString('aa)) / result.meanV(SwitchString('a))
    val ratioMax = result.maxV(SwitchString('aa)) / result.maxV(SwitchString('a))
    print(ratio(3), ratioMean, ratioMax)
  }

  def print(ratio: Double, ratioMean: Double, ratioMax: Double, p: Boolean = false) = {
    val s = if (p) "%" else ""
    myprinter(0).println(s + "(" + ratio + "," + ratioMean + ")")
    myprinter(1).println(s + "(" + ratio + "," + ratioMax + ")")
  }

  def close = myprinter.foreach(_.close)
}

case class PrintPointsRatioDegree(val nameBench: String) {
  val base = "output/tikz/corrDegree"
  val myprinter = Array("Max", "Mean").map { m =>
    Array("Less", "Equal", "More", "ForAllEqu").map { h =>
      new PrintWriter(base + h + "-" + nameBench + "-" + m)
    } // less : node with balanced out greater
    // more : node with balanced in greater
  }

  def feed(graph: AbstractGraph, result: ResultIndividualGraph): Unit = {
    val ratio = graph.ratios
    val ratioMean = result.meanV(SwitchString('aa)) / result.meanV(SwitchString('a))
    val ratioMax = result.maxV(SwitchString('aa)) / result.maxV(SwitchString('a))
    print(ratio(3), ratio(4), ratioMean, ratioMax, ratioMax >= 20 ||
      ratioMean >= 20 ||
      ratio(3) >= 20 || Random.nextInt(100) <= 80)
  }

  def feedAll(graph: AbstractGraph, result: ResultIndividualGraph): Unit = {
    val ratio = graph.ratios
    val ratioMean = result.meanV(SwitchString('aa)) / result.meanV(SwitchString('a))
    val ratioMax = result.maxV(SwitchString('aa)) / result.maxV(SwitchString('a))
    print(ratio(3), ratio(4), ratioMean, ratioMax)
  }

  def print(ratio: Double, ratio2: Double, ratioMean: Double, ratioMax: Double, p: Boolean = false) = {
    val s = if (p) "%" else ""
    if (ratio2 == 1.0) {
      myprinter(0)(1).println(s + "(" + ratio + "," + ratioMean + ")")
      myprinter(1)(1).println(s + "(" + ratio + "," + ratioMax + ")")
    } else if (ratio2 == 0.0) {
      myprinter(0)(0).println(s + "(" + ratio + "," + ratioMean + ")")
      myprinter(1)(0).println(s + "(" + ratio + "," + ratioMax + ")")
    } else if (ratio2 == 2.0) {
      myprinter(0)(2).println(s + "(" + ratio + "," + ratioMean + ")")
      myprinter(1)(2).println(s + "(" + ratio + "," + ratioMax + ")")
    } else {
      myprinter(0)(3).println(s + "(" + ratio + "," + ratioMean + ")")
      myprinter(1)(3).println(s + "(" + ratio + "," + ratioMax + ")")
    }

  }

  def close = myprinter.foreach(_.foreach(_.close))
}

