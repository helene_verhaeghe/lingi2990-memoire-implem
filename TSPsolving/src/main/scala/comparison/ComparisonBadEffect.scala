/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import tsplib.TSPlib
import heldKarpSymmetric.HeldKarpBound
import heldKarpSymmetric.Geometric
import heldKarpSymmetric.OverK
import myVisu.ResultGraph
import printer.TikzGraphicPrinter

/**
 * Classes to show the drawbacks of each step size
 */
object ComparisonBadEffectHK extends App {

  val tspgraph = TSPlib.br17
  val graph = tspgraph.toGraph

  val param = Array(0, 25, 50, 100, 250, 500)
  val fct = param.map(x => () => new HeldKarpBound(tspgraph.optimal + x, graph.listVertice.size))

  val nbit = 80

  val resultGraph = ResultGraph("Held Karp Asymetric of " + tspgraph.instance_name, "iteration", "bound", 0, nbit * 1.05)
  resultGraph.getHorizontalLine("optimal", tspgraph.optimal)
  val line_asym = param.map(x => resultGraph.getEvolutiveLine("optimal +" + x))

  resultGraph.create

  val res = param.zipWithIndex.map(p => graph.toEdmondsGraph.bound(fct(p._2)(), 0, Some(nbit), line_asym(p._2).addTo))

  TikzGraphicPrinter("iter-badeffect-HK", resultGraph)
}

object ComparisonBadEffectGeom extends App {

  val tspgraph = TSPlib.br17
  val graph = tspgraph.toGraph

  val param1 = Array(0.1, 0.4, 0.7, 0.9)
  val fct1 = param1.map(x => () => new Geometric(1.0, x))

  val param2 = Array(0.1, 5, 10, 15)
  val fct2 = param2.map(x => () => new Geometric(x, 0.9))

  val nbit = 80

  val resultGraph = ResultGraph("Held Karp Asymetric of " + tspgraph.instance_name, "iteration", "bound", 0, nbit * 1.05)
  resultGraph.getHorizontalLine("optimal", tspgraph.optimal)
  val line_asym1 = param1.map(x => resultGraph.getEvolutiveLine("$\\beta_0 = 1.0$ \\& $\\rho = " + x + "$"))
  val line_asym2 = param2.map(x => resultGraph.getEvolutiveLine("$\\beta_0 = " + x + "$ \\& $\\rho = 0.9$"))

  resultGraph.create

  val res1 = param1.zipWithIndex.map(p => graph.toEdmondsGraph.bound(fct1(p._2)(), 0, Some(nbit), line_asym1(p._2).addTo))
  val res2 = param2.zipWithIndex.map(p => graph.toEdmondsGraph.bound(fct2(p._2)(), 0, Some(nbit), line_asym2(p._2).addTo))

  TikzGraphicPrinter("iter-badeffect-geom", resultGraph)
}

object ComparisonBadEffectOverK extends App {

  val tspgraph = TSPlib.br17
  val graph = tspgraph.toGraph

  val param1 = Array(0.5, 1.0, 3.0, 5.0, 10.0, 15)
  val fct1 = param1.map(x => () => new OverK(x))

  val nbit = 80

  val resultGraph = ResultGraph("Held Karp Asymetric of " + tspgraph.instance_name, "iteration", "bound", 0, nbit * 1.05)
  resultGraph.getHorizontalLine("optimal", tspgraph.optimal)
  val line_asym1 = param1.map(x => resultGraph.getEvolutiveLine("$" + x + "/k$"))

  resultGraph.create

  val res1 = param1.zipWithIndex.map(p => graph.toEdmondsGraph.bound(fct1(p._2)(), 0, Some(nbit), line_asym1(p._2).addTo))

  TikzGraphicPrinter("iter-badeffect-overk", resultGraph)
}