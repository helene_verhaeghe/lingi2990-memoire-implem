/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import graph._
import printer._
import tsplib.TSPlib
import linearRelaxationTSP._
import scala.collection.mutable.Map
import branchAndBoundTSP.BranchAndBoundATSP

/**
 * Compare Asymmetric Linear relaxation and
 * symmetric linear relaxation
 */
object ComparisonRandom extends App {

  implicit val threshold = 0.00001
  val IDTEST = System.currentTimeMillis() + "_force"

  val ALL = ResultPrinter("output/" + IDTEST + "_all")
  val BAD = ResultPrinter("output/" + IDTEST + "_bad_nothappening", res => res.isAsymBetterThanOptimal || res.isSym3BetterThanAsym || res.isSym2BetterThanAsym)
  val SYM2LESS = ResultPrinter("output/" + IDTEST + "_sym2less", res => res.isAsymBetterThanSym2)
  val SYM3LESS = ResultPrinter("output/" + IDTEST + "_sym3less", res => res.isAsymBetterThanSym3)
  val NOTTIGHT = ResultPrinter("output/" + IDTEST + "_nottight", res => !(res.isAsymOptimal && res.isSym2Optimal && res.isSym3Optimal))

  var id = 0
  for (nbnode <- 10 to 20; pourcent <- 50 to 100 by 10; maxcost <- 20 to 100 by 20; nbgraph <- 1 to 20) {
    id += 1
    val idTag = "" + IDTEST + "-" + id
    val graph = GraphGenerator.gen(idTag, nbnode, maxcost, pourcent)
    println(graph.name)
    val testingResults = AllTesting.testForce(graph, false)
    if (testingResults.hasTSP) {
      val testingResultsTSP = testingResults.asInstanceOf[AllTestingResultTSP]
      ALL.feed(id, testingResultsTSP)
      BAD.feedComplete(id, testingResultsTSP)
      SYM2LESS.feedComplete(id, testingResultsTSP)
      SYM3LESS.feedComplete(id, testingResultsTSP)
      NOTTIGHT.feedComplete(id, testingResultsTSP)
    }

  }

  ALL.close
  BAD.close
  SYM2LESS.close
  SYM3LESS.close
  NOTTIGHT.close
}

object ComparisonTSPLib extends App {

  implicit val threshold = 0.00001
  val IDTEST = System.currentTimeMillis() + "_tsplib"

  val ALL = ResultPrinter("output/" + IDTEST + "_all")
  val BAD = ResultPrinter("output/" + IDTEST + "_bad_nothappening", res => res.isAsymBetterThanOptimal || res.isSym3BetterThanAsym || res.isSym2BetterThanAsym)
  val SYM2LESS = ResultPrinter("output/" + IDTEST + "_sym2less", res => res.isAsymBetterThanSym2)
  val SYM3LESS = ResultPrinter("output/" + IDTEST + "_sym3less", res => res.isAsymBetterThanSym3)
  val NOTTIGHT = ResultPrinter("output/" + IDTEST + "_nottight", res => !(res.isAsymOptimal && res.isSym2Optimal && res.isSym3Optimal))

  var id = 0
  for (tsplibgraph <- List(TSPlib.br17)) {
    id += 1
    val idTag = "" + IDTEST + "-" + id
    val graph = tsplibgraph.toGraph
    println(graph.name)
    val testingResults = AllTestingResultTSP(graph,
      SubGraphTSP(graph, Some(tsplibgraph.optimal), Map(), BranchAndBoundATSP), //TODO
      LinearRelaxationTSPasymmetric(graph),
      LinearRelaxationTSPsymmetricWithForce(graph.getSymmetricVersion(0)),
      None)
    if (testingResults.hasTSP) {
      val testingResultsTSP = testingResults.asInstanceOf[AllTestingResultTSP]
      ALL.feed(id, testingResultsTSP)
      BAD.feedComplete(id, testingResultsTSP)
      SYM2LESS.feedComplete(id, testingResultsTSP)
      SYM3LESS.feedComplete(id, testingResultsTSP)
      NOTTIGHT.feedComplete(id, testingResultsTSP)
    }

  }

  ALL.close
  BAD.close
  SYM2LESS.close
  SYM3LESS.close
  NOTTIGHT.close
}
