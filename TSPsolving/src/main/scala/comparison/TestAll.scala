/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import graph._
import linearRelaxationTSP._
import branchAndBoundTSP._
import tsplib._

/**
 * Apply all the tests (linear relaxs) to one simple graph
 * the graph must at least have a tsp solution
 */
object AllTesting {
  def apply(graph: AbstractGraph, withSym3: Boolean = true): AllTestingResult = {
    val resBB = TSPBranchBound(graph)
    if (resBB.value.isEmpty) {
      AllTestingResultNoTSP(graph, resBB)
    } else {
      AllTestingResultTSP(graph,
        resBB,
        LinearRelaxationTSPasymmetric(graph),
        LinearRelaxationTSPsymmetric(graph.getSymmetricVersion(-1000)),
        if (withSym3) { Some(LinearRelaxationTSPsymmetric(graph.getTriSymmetricVersion)) } else { None })
    }
  }
  def testForce(graph: AbstractGraph, withSym3: Boolean = true): AllTestingResult = {
    val resBB = TSPBranchBound(graph)
    if (resBB.value.isEmpty) {
      AllTestingResultNoTSP(graph, resBB)
    } else {
      AllTestingResultTSP(graph,
        resBB,
        LinearRelaxationTSPasymmetric(graph),
        LinearRelaxationTSPsymmetricWithForce(graph.getSymmetricVersion(0)),
        if (withSym3) { Some(LinearRelaxationTSPsymmetric(graph.getTriSymmetricVersion)) } else { None })
    }
  }
}

trait AllTestingResult {
  def hasTSP: Boolean
}

case class AllTestingResultNoTSP(val graph: AbstractGraph, val subgraphBB: SubGraphTSP) extends AllTestingResult {
  def hasTSP = false
}

case class AllTestingResultTSP(
  val graph: AbstractGraph,
  val subgraphBB: SubGraphTSP,
  val subgraphLRasym: SubGraph,
  val subgraphLRsym2: SubGraph,
  val subgraphLRsym3: Option[SubGraph]) extends AllTestingResult {

  def hasTSP = true

  def isAsymOptimal(implicit threshold: Double = 0.001): Boolean = {
    Math.abs(subgraphBB.value.get - subgraphLRasym.value.get) < threshold
  }
  def isSym2Optimal(implicit threshold: Double = 0.001): Boolean = {
    Math.abs(subgraphBB.value.get - subgraphLRsym2.value.get) < threshold
  }
  def isSym3Optimal(implicit threshold: Double = 0.001): Boolean = {
    !subgraphLRsym3.isEmpty && Math.abs(subgraphBB.value.get - subgraphLRsym3.get.value.get) < threshold
  }

  // Bound the same if true
  def isSym2EqualAsym(implicit threshold: Double = 0.001): Boolean = {
    Math.abs(subgraphLRsym2.value.get - subgraphLRasym.value.get) < threshold
  }

  // Bound the same if true
  def isSym3EqualAsym(implicit threshold: Double = 0.001): Boolean = {
    !subgraphLRsym3.isEmpty && Math.abs(subgraphLRsym3.get.value.get - subgraphLRasym.value.get) < threshold
  }

  // if true, need to create specific thing for asym
  def isAsymBetterThanSym2(implicit threshold: Double = 0.001): Boolean = {
    (subgraphLRsym2.value.get - subgraphLRasym.value.get < -threshold)
  }

  // if true, need to create specific thing for asym
  def isAsymBetterThanSym3(implicit threshold: Double = 0.001): Boolean = {
    !subgraphLRsym3.isEmpty && (subgraphLRsym3.get.value.get - subgraphLRasym.value.get < -threshold)
  }

  // not good if true, normally never true
  def isAsymBetterThanOptimal(implicit threshold: Double = 0.001): Boolean = {
    (subgraphBB.value.get - subgraphLRasym.value.get < -threshold)
  }

  // not good if true, normally never true
  def isSym2BetterThanAsym(implicit threshold: Double = 0.001): Boolean = {
    (subgraphLRasym.value.get - subgraphLRsym2.value.get < -threshold)
  }

  // not good if true, normally never true
  def isSym3BetterThanAsym(implicit threshold: Double = 0.001): Boolean = {
    !subgraphLRsym3.isEmpty && (subgraphLRasym.value.get - subgraphLRsym3.get.value.get < -threshold)
  }

  override def toString: String = {
    this.asString(_.shortString, _.shortString)
  }

  def toStringComplete: String = {
    this.asString(_.toString, _.toString)
  }

  def asString(meth: SubGraph => String, meth2: SubGraphTSP => String): String = {
    "********************\n" +
      "********************\n" +
      "========GRAPH========\n" +
      graph + "\n\n" +
      "====STRING OBJECT====\n" +
      graph.toStringObject + "\n\n" +
      "=======RESULTS=======\n" +
      "\n<<<<<<<<<<1>>>>>>>>>>\n\n" +
      meth2(subgraphBB) + "\n" +
      "\n<<<<<<<<<<2>>>>>>>>>>\n\n" +
      meth(subgraphLRasym) + "\n" +
      "\n<<<<<<<<<<3>>>>>>>>>>\n\n" +
      meth(subgraphLRsym2) + "\n" +
      "\n<<<<<<<<<<4>>>>>>>>>>\n\n" +
      (if (subgraphLRsym3.isEmpty) { "Not Tested" } else {
        meth(subgraphLRsym3.get)
      }) + "\n" +
      "********************\n" +
      "********************"
  }
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */
object TestAllTesting extends App {

  val graph = GraphCollection.graph13
  //  val graph = TSPlib.br17.toGraph
  val result = AllTesting(graph)
  println(result.asInstanceOf[AllTestingResultTSP].toStringComplete)

}
