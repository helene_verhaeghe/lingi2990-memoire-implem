/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import graph._
import branchAndBoundTSP.TSPBranchBound
import heldKarpSymmetric.OverK
import heldKarpSymmetric.WeightedFunction
import heldKarpSymmetric.Geometric
import heldKarpSymmetric.HeldKarpBound
import tsplib.TSPlib

/**
 * Comparison of the iteration
 * for different bounds and different benchmark
 */
object ComparisonIterRandA extends App {

  val randA = RandomSeed("RandA", 3 to 17, 10 to 100 by 10, 20 to 100 by 20, 20)
  val randAbis = RandomSeed("RandA", 3 to 10, 10 to 100 by 10, 20 to 100 by 20, 20)

  val randomseed = randA

  implicit val threshold = 0.00001
  val nameBench = randomseed.name

  val IDTEST = System.currentTimeMillis() + "_start"

  val meth = Array("1Tree 100", "1Tree 200", "1Arbo", "1Anti", "pred")
  // ===============
  // graph creation
  val graph1_pptime = PP(nameBench, "over k", meth, 10)
  val graph2_pptime = PP(nameBench, "geometric", meth, 10)
  val graph3_pptime = PP(nameBench, "complex", meth, 10)
  val graph1_ppoptim = PP(nameBench, "over k", meth, 10, true)
  val graph2_ppoptim = PP(nameBench, "geometric", meth, 10, true)
  val graph3_ppoptim = PP(nameBench, "complex", meth, 10, true)

  val setGraph = Set(graph1_pptime,
    graph2_pptime,
    graph3_pptime,
    graph1_ppoptim,
    graph2_ppoptim,
    graph3_ppoptim)

  setGraph.foreach(_.create)
  // ===============

  var id = 0

  for (nbnode <- randomseed.sizeRange) {
    println(nbnode)
    for {
      pourcent <- randomseed.pourcentRange;
      maxcost <- randomseed.costRange;
      nbgraph <- 1 to randomseed.nbgraph
    } {
      id += 1
      val idTag = "" + IDTEST + "-" + id
      val graph = GraphGenerator.gen(idTag, nbnode, maxcost, pourcent)
      val resBB = TSPBranchBound(graph)
      if (!resBB.value.isEmpty) {
        val ub = graph.ub
        def gen(step: () => WeightedFunction, time_pp: PP, optim_pp: PP): (Array[Double], Array[Double]) = {
          val times1: Array[Double] = Array.fill(5)(0.0)
          val optim1: Array[Double] = Array.fill(5)(0.0)

          TimeSaver.setOffset
          optim1(0) = graph.getSymmetricVersion.toHKgraph.bound(step(), 0, Some(100), (x, y) => Unit, true)
          times1(0) = TimeSaver.timePassed

          TimeSaver.setOffset
          optim1(1) = graph.getSymmetricVersion.toHKgraph.bound(step(), 0, Some(200), (x, y) => Unit, true)
          times1(1) = TimeSaver.timePassed

          TimeSaver.setOffset
          optim1(2) = graph.toEdmondsGraph.bound(step(), 0, Some(100), (x, y) => Unit, true)
          times1(2) = TimeSaver.timePassed

          TimeSaver.setOffset
          optim1(3) = graph.toEdmondsGraph.bound(step(), 0, Some(100), (x, y) => Unit, false)
          times1(3) = TimeSaver.timePassed

          TimeSaver.setOffset
          val pred = graph.predictorBetterUseArbo
          times1(4) = TimeSaver.timePassed

          if (pred) {
            times1(4) += times1(2)
            optim1(4) = optim1(2)
          } else {
            times1(4) += times1(3)
            optim1(4) = optim1(3)
          }

          time_pp.addData(times1)
          optim_pp.addData(optim1)
          (times1, optim1)
        }

        val (res_time1, res_optim1) = gen(() => OverK(50), graph1_pptime, graph1_ppoptim)
        val (res_time2, res_optim2) = gen(() => Geometric(100, 0.9), graph2_pptime, graph2_ppoptim)
        val (res_time3, res_optim3) = gen(() => HeldKarpBound(ub, nbnode), graph3_pptime, graph3_ppoptim)

      }

    }

  }
  println("end computation")
  setGraph.foreach(_.add)

  // ===============
  // graph print
  import printer._
  TikzGraphicPrinter(nameBench + "-graph1-iter-pptime", graph1_pptime.graph)
  TikzGraphicPrinter(nameBench + "-graph2-iter-pptime", graph2_pptime.graph)
  TikzGraphicPrinter(nameBench + "-graph3-iter-pptime", graph3_pptime.graph)
  TikzGraphicPrinter(nameBench + "-graph1-iter-ppoptim", graph1_ppoptim.graph)
  TikzGraphicPrinter(nameBench + "-graph2-iter-ppoptim", graph2_ppoptim.graph)
  TikzGraphicPrinter(nameBench + "-graph3-iter-ppoptim", graph3_ppoptim.graph)

  // ===============

}

object ComparisonIterRandB extends App {

  val randB = RandomSeed("RandB", 20 to 50, 100 to 100, 100 to 1000 by 100, 20)
  val randBbis = RandomSeed("RandB", 20 to 30, 100 to 100, 100 to 1000 by 100, 20)

  ComparisonIterRandomFull(randB, (s, i1, i2, i3) => GraphGenerator.gen(s, i1, i2, i3))

}
object ComparisonIterRandC extends App {

  val randC = RandomSeed("RandC", 20 to 50, 100 to 100, 100 to 1000 by 100, 20)
  val randCbis = RandomSeed("RandC", 20 to 30, 100 to 100, 100 to 1000 by 100, 20)

  ComparisonIterRandomFull(randC, (s, i1, i2, i3) => GraphGenerator.genBis(s, i1, i2, i3))

}

object ComparisonIterRandomFull extends App {

  def apply(randomseed: RandomSeed, genmeth: (String, Int, Int, Int) => AbstractGraph) = {

    implicit val threshold = 0.00001
    val nameBench = randomseed.name

    val IDTEST = System.currentTimeMillis() + "_start"

    val meth = Array("1Tree 100", "1Tree 200", "1Arbo", "1Anti", "pred")
    // ===============
    // graph creation
    val graph1_pptime = PP(nameBench, "over k", meth, 10)
    val graph2_pptime = PP(nameBench, "geometric", meth, 10)
    val graph3_pptime = PP(nameBench, "complex", meth, 10)
    val graph1_ppoptim = PP(nameBench, "over k", meth, 10, true)
    val graph2_ppoptim = PP(nameBench, "geometric", meth, 10, true)
    val graph3_ppoptim = PP(nameBench, "complex", meth, 10, true)

    val setGraph = Set(graph1_pptime,
      graph2_pptime,
      graph3_pptime,
      graph1_ppoptim,
      graph2_ppoptim,
      graph3_ppoptim)

    setGraph.foreach(_.create)
    // ===============

    var id = 0

    for (nbnode <- randomseed.sizeRange) {
      println(nbnode)
      for {
        pourcent <- randomseed.pourcentRange;
        maxcost <- randomseed.costRange;
        nbgraph <- 1 to randomseed.nbgraph
      } {
        id += 1
        val idTag = "" + IDTEST + "-" + id
        val graph = genmeth(idTag, nbnode, maxcost, pourcent)
        val ub = graph.ub

        def gen(step: () => WeightedFunction, time_pp: PP, optim_pp: PP): (Array[Double], Array[Double]) = {
          val times1: Array[Double] = Array.fill(5)(0.0)
          val optim1: Array[Double] = Array.fill(5)(0.0)

          TimeSaver.setOffset
          optim1(0) = graph.getSymmetricVersion.toHKgraph.bound(step(), 0, Some(100), (x, y) => Unit, true)
          times1(0) = TimeSaver.timePassed

          TimeSaver.setOffset
          optim1(1) = graph.getSymmetricVersion.toHKgraph.bound(step(), 0, Some(200), (x, y) => Unit, true)
          times1(1) = TimeSaver.timePassed

          TimeSaver.setOffset
          optim1(2) = graph.toEdmondsGraph.bound(step(), 0, Some(100), (x, y) => Unit, true)
          times1(2) = TimeSaver.timePassed

          TimeSaver.setOffset
          optim1(3) = graph.toEdmondsGraph.bound(step(), 0, Some(100), (x, y) => Unit, false)
          times1(3) = TimeSaver.timePassed

          TimeSaver.setOffset
          val pred = graph.predictorBetterUseArbo
          times1(4) = TimeSaver.timePassed

          if (pred) {
            times1(4) += times1(2)
            optim1(4) = optim1(2)
          } else {
            times1(4) += times1(3)
            optim1(4) = optim1(3)
          }

          time_pp.addData(times1)
          optim_pp.addData(optim1)
          (times1, optim1)
        }

        val (res_time1, res_optim1) = gen(() => OverK(50), graph1_pptime, graph1_ppoptim)
        val (res_time2, res_optim2) = gen(() => Geometric(100, 0.9), graph2_pptime, graph2_ppoptim)
        val (res_time3, res_optim3) = gen(() => HeldKarpBound(ub, nbnode), graph3_pptime, graph3_ppoptim)

      }

    }
    println("end computation")
    setGraph.foreach(_.add)

    // ===============
    // graph print
    import printer._
    TikzGraphicPrinter(nameBench + "-graph1-iter-pptime", graph1_pptime.graph)
    TikzGraphicPrinter(nameBench + "-graph2-iter-pptime", graph2_pptime.graph)
    TikzGraphicPrinter(nameBench + "-graph3-iter-pptime", graph3_pptime.graph)
    TikzGraphicPrinter(nameBench + "-graph1-iter-ppoptim", graph1_ppoptim.graph)
    TikzGraphicPrinter(nameBench + "-graph2-iter-ppoptim", graph2_ppoptim.graph)
    TikzGraphicPrinter(nameBench + "-graph3-iter-ppoptim", graph3_ppoptim.graph)
    // ===============
  }
}

object ComparisonIterTSPlib extends App {

  implicit val threshold = 0.00001
  val nameBench = "TSPlib"

  val tsplib = TSPlib.ALL_TSPlib.sortBy(_.instance_dimension)

  val meth = Array("1Tree 100", "1Tree 200", "1Arbo", "1Anti", "pred")
  // ===============
  // graph creation
  val graph1_pptime = PP(nameBench, "over k", meth, 100)
  val graph2_pptime = PP(nameBench, "geometric", meth, 100)
  val graph3_pptime = PP(nameBench, "complex", meth, 100)
  val graph1_ppoptim = PP(nameBench, "over k", meth, 100, true)
  val graph2_ppoptim = PP(nameBench, "geometric", meth, 100, true)
  val graph3_ppoptim = PP(nameBench, "complex", meth, 100, true)

  val setGraph = Set(graph1_pptime,
    graph2_pptime,
    graph3_pptime,
    graph1_ppoptim,
    graph2_ppoptim,
    graph3_ppoptim)

  setGraph.foreach(_.create)
  // ===============

  for (graphTSPlib <- tsplib) { //; if graphTSPlib.instance_dimension <= 75) {
    println(graphTSPlib.instance_name)
    val graph = graphTSPlib.toGraph
    val nbnode = graphTSPlib.instance_dimension
    val ub = graph.ub

    def gen(step: () => WeightedFunction, time_pp: PP, optim_pp: PP): (Array[Double], Array[Double]) = {
      val times1: Array[Double] = Array.fill(5)(0.0)
      val optim1: Array[Double] = Array.fill(5)(0.0)

      TimeSaver.setOffset
      optim1(0) = graph.getSymmetricVersion.toHKgraph.bound(step(), 0, Some(100), (x, y) => Unit, true)
      times1(0) = TimeSaver.timePassed

      TimeSaver.setOffset
      optim1(1) = graph.getSymmetricVersion.toHKgraph.bound(step(), 0, Some(200), (x, y) => Unit, true)
      times1(1) = TimeSaver.timePassed

      TimeSaver.setOffset
      optim1(2) = graph.toEdmondsGraph.bound(step(), 0, Some(100), (x, y) => Unit, true)
      times1(2) = TimeSaver.timePassed

      TimeSaver.setOffset
      optim1(3) = graph.toEdmondsGraph.bound(step(), 0, Some(100), (x, y) => Unit, false)
      times1(3) = TimeSaver.timePassed

      TimeSaver.setOffset
      val pred = graph.predictorBetterUseArbo
      times1(4) = TimeSaver.timePassed

      if (pred) {
        times1(4) += times1(2)
        optim1(4) = optim1(2)
      } else {
        times1(4) += times1(3)
        optim1(4) = optim1(3)
      }

      time_pp.addData(times1)
      optim_pp.addData(optim1)
      (times1, optim1)
    }

    val (res_time1, res_optim1) = gen(() => OverK(50), graph1_pptime, graph1_ppoptim)
    val (res_time2, res_optim2) = gen(() => Geometric(100, 0.9), graph2_pptime, graph2_ppoptim)
    val (res_time3, res_optim3) = gen(() => HeldKarpBound(ub, nbnode), graph3_pptime, graph3_ppoptim)

  }
  println("end computation")
  setGraph.foreach(_.add)

  // ===============
  // graph print
  import printer._
  TikzGraphicPrinter(nameBench + "-graph1-iter-pptime", graph1_pptime.graph)
  TikzGraphicPrinter(nameBench + "-graph2-iter-pptime", graph2_pptime.graph)
  TikzGraphicPrinter(nameBench + "-graph3-iter-pptime", graph3_pptime.graph)
  TikzGraphicPrinter(nameBench + "-graph1-iter-ppoptim", graph1_ppoptim.graph)
  TikzGraphicPrinter(nameBench + "-graph2-iter-ppoptim", graph2_ppoptim.graph)
  TikzGraphicPrinter(nameBench + "-graph3-iter-ppoptim", graph3_ppoptim.graph)
  // ===============

}