/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import tsplib.TSPlib
import heldKarpSymmetric.Geometric
import myVisu.ResultGraph
import printer.TikzGraphicPrinter

/**
 * Show that a bound good bound for one
 * is possibly not good for other
 */
object ComparisonBestForAll extends App {
  val tspgraph1 = TSPlib.ft53
  val graph1 = tspgraph1.toGraph
  val tspgraph2 = TSPlib.ft70
  val graph2 = tspgraph1.toGraph
  val tspgraph3 = TSPlib.ry48p
  val graph3 = tspgraph1.toGraph

  val fct = () => new Geometric(100, 0.9)

  val nbit = 80

  val resultGraph = ResultGraph("Held Karp Asymetric", "iteration", "\\frac{bound}{optimal\\ solution}", 0, nbit * 1.05)
  resultGraph.getHorizontalLine("optimal", 1.0)
  val line_asym1 = resultGraph.getEvolutiveLine(tspgraph1.instance_name)
  val line_asym2 = resultGraph.getEvolutiveLine(tspgraph2.instance_name)
  val line_asym3 = resultGraph.getEvolutiveLine(tspgraph3.instance_name)

  resultGraph.create

  val res1 = graph1.toEdmondsGraph.bound(fct(), 0, Some(nbit), (x, y) => line_asym1.addTo(x, y / tspgraph1.optimal))
  val res2 = graph2.toEdmondsGraph.bound(fct(), 0, Some(nbit), (x, y) => line_asym2.addTo(x, y / tspgraph2.optimal))
  val res3 = graph3.toEdmondsGraph.bound(fct(), 0, Some(nbit), (x, y) => line_asym3.addTo(x, y / tspgraph3.optimal))

  TikzGraphicPrinter("iter-param-notall", resultGraph)
}