/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package comparison

import graph._
import branchAndBoundTSP.TSPBranchBound
import scala.collection.mutable.HashMap
import scala.collection.mutable.Set
import myVisu._
import tsplib.TSPlib
import java.io.PrintWriter
import printer.TikzGraphicPrinter

/**
 * Compare the 1Tree's, 1Arbo's and 1Anti's for TSPlib
 */
object ComparisonStartTSPlib extends App {

  implicit val threshold = 0.00001
  val nameBench = "TSPlib"
  val myprinter = PrintPointsRatioVar(nameBench)
  //  val myprinterD = PrintPointsRatioDegree(nameBench)

  val tsplib = TSPlib.ALL_TSPlib.sortBy(_.instance_dimension)
  val rangeNbNode = tsplib.head.instance_dimension to tsplib.last.instance_dimension
  val rg = ResultGatherer(rangeNbNode)

  // ===============
  // graph creation

  val graph4_ArboVsTree_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'a, 't).create
  val graph4_AntiVsTree_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'aa, 't).create
  val graph4_ArboVsAnti_pp = PerfProfileGraphByGraphEachNode(nameBench, rg, 'a, 'aa).create

  val graph_pp = PP(nameBench, "", Array("1Arbo", "1Tree"), 30)
  graph_pp.create
  val graph_pp_only = PP(nameBench, "", Array("1Arbo", "1Tree"), 30)
  graph_pp_only.create

  // ===============

  for (graphTSPlib <- tsplib) { //; if graphTSPlib.instance_dimension <= 75) {
    val graph = graphTSPlib.toGraph
    val nbnode = graphTSPlib.instance_dimension

    val time: Array[Long] = Array.fill(4)(0)

    TimeSaver.setOffset
    val a = graph.toEdmondsGraph
    time(0) = TimeSaver.timePassed
    TimeSaver.setOffset
    val a2 = a.all1arbo
    time(1) = TimeSaver.timePassed

    TimeSaver.setOffset
    val c = graph.getSymmetricVersion.toHKgraph
    time(2) = TimeSaver.timePassed
    TimeSaver.setOffset
    val c2 = c.all1Tree(true)
    time(3) = TimeSaver.timePassed

    graph_pp.addData(Array(time(0) + 1.0 * time(1) / nbnode, time(2) + 0.5 * time(3) / nbnode))
    graph_pp_only.addData(Array(1.0 * time(1) / nbnode, 0.5 * time(3) / nbnode))

    val result = ResultIndividualGraph(
      nbnode,
      graphTSPlib.optimal,
      a2,
      graph.getInvertedVersion.toEdmondsGraph.all1arbo,
      c2)
    rg.add(result)

    println(graphTSPlib.instance_name)

    myprinter.feedAll(graph, result)
    //    myprinterD.feedAll(graph, result)
  }
  graph4_ArboVsTree_pp.add
  graph4_AntiVsTree_pp.add
  graph4_ArboVsAnti_pp.add
  graph_pp.add
  graph_pp_only.add

  println("graph1-1")
  println(rg.countRatioAll('a, 't, 'mean, false))
  println(rg.countRatioAll('a, 't, 'max, false))
  println(rg.countRatioAll('a, 't, 'min, false))
  println(rg.countRatioAll('a, 't, 'var, false))
  println("graph1-2")
  println(rg.countRatioAll('a, 't, 'mean, true))
  println(rg.countRatioAll('a, 't, 'max, true))
  println(rg.countRatioAll('a, 't, 'min, true))
  println(rg.countRatioAll('a, 't, 'var, true))
  println("graph1-3")
  println(rg.countRatioAllMinMax('a, 't, false))
  println(rg.countRatioAllMinMax('a, 't, true))
  println("graph2-1")
  println(rg.countRatioAll('aa, 't, 'mean, false))
  println(rg.countRatioAll('aa, 't, 'max, false))
  println(rg.countRatioAll('aa, 't, 'min, false))
  println(rg.countRatioAll('aa, 't, 'var, false))
  println("graph2-2")
  println(rg.countRatioAll('aa, 't, 'mean, true))
  println(rg.countRatioAll('aa, 't, 'max, true))
  println(rg.countRatioAll('aa, 't, 'min, true))
  println(rg.countRatioAll('aa, 't, 'var, true))
  println("graph2-3")
  println(rg.countRatioAllMinMax('aa, 't, false))
  println(rg.countRatioAllMinMax('aa, 't, true))
  println("graph3-1")
  println(rg.countRatioAll('a, 'aa, 'mean, false))
  println(rg.countRatioAll('a, 'aa, 'max, false))
  println(rg.countRatioAll('a, 'aa, 'min, false))
  println(rg.countRatioAll('a, 'aa, 'var, false))
  println("graph3-2")
  println(rg.countRatioAll('a, 'aa, 'mean, true))
  println(rg.countRatioAll('a, 'aa, 'max, true))
  println(rg.countRatioAll('a, 'aa, 'min, true))
  println(rg.countRatioAll('a, 'aa, 'var, true))
  println("graph3-3")
  println(rg.countRatioAllMinMax('a, 'aa, false))
  println(rg.countRatioAllMinMax('a, 'aa, true))
  println("graph3-4")
  println(rg.countRatioAllMinMax('aa, 'a, false))
  println(rg.countRatioAllMinMax('aa, 'a, true))

  TikzGraphicPrinter(nameBench + "-graph4-ArboVsTree-pp", graph4_ArboVsTree_pp.graph)
  TikzGraphicPrinter(nameBench + "-graph4-AntiVsTree-pp", graph4_AntiVsTree_pp.graph)
  TikzGraphicPrinter(nameBench + "-graph4-ArboVsAnti-pp", graph4_ArboVsAnti_pp.graph)
  TikzGraphicPrinter(nameBench + "-graph5-pptime-full", graph_pp.graph)
  TikzGraphicPrinter(nameBench + "-graph5-pptime-only", graph_pp_only.graph)

  myprinter.close
  //  myprinterD.close
}