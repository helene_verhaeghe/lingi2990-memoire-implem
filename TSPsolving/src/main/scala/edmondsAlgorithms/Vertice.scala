/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edmondsAlgorithms

import scala.collection.mutable.HashSet

class Vertice(val id: Int, val level: Int = 0)(var modifyWeight: Double = 0.0) {
  val inEdge: HashSet[Edge] = HashSet()
  val outEdge: HashSet[Edge] = HashSet()

  var prevLvl: Option[HashSet[Vertice]] = None
  var internal: Option[HashSet[Edge]] = None

  def updateWeight(beta: Double) = {
    val degree = inEdge.count(e => e.isSelected) + outEdge.count(e => e.isSelected)
    modifyWeight += (2 - degree) * beta
  }

  def reset = {
    this.prevLvl = None
    this.internal = None
  }

  def addIn(edge: Edge) =
    inEdge += edge

  def addOut(edge: Edge) =
    outEdge += edge

  /**
   * Reject all in/out edge
   */
  def rejectInEdges = rejectEdge(this.inEdge)
  def rejectOutEdges = rejectEdge(this.outEdge)
  private def rejectEdge(setEdge: HashSet[Edge]) =
    for (edge <- setEdge) { edge.reject }

  /**
   * Reject all unselected in/out edge
   */
  def rejectUnselectedInEdges = rejectUnselected(this.inEdge)
  def rejectUnselectedOutEdges = rejectUnselected(this.outEdge)
  private def rejectUnselected(setEdge: HashSet[Edge]) =
    for (edge <- setEdge; if !edge.isSelected) { edge.reject }

  /**
   * Reduce the cost of in/out edge
   */
  def reduceCostSelectedInEdge =
    reduceCostSelected(this.inEdge)
  def reduceCostSelectedOutEdge =
    reduceCostSelected(this.outEdge)
  private def reduceCostSelected(setEdge: HashSet[Edge]) = {
    val filteredEdge = setEdge.filter(e => e.isSelected)
    if (!filteredEdge.isEmpty) {
      val mincost = filteredEdge.minBy(_.reduceCost).reduceCost
      for (edge <- filteredEdge) { edge.reduceCost(mincost) }
    }
  }

  /**
   * Select one in/out edge
   */
  def selectOneInEdge = selectOne(this.inEdge)
  def selectOneOutEdge = selectOne(this.outEdge)
  private def selectOne(setEdge: HashSet[Edge]) = {
    val filteredZeroEdge = setEdge.filter(e => e.isSelected && e.reduceCost == 0)
    if (!filteredZeroEdge.isEmpty) {
      for (edge <- filteredZeroEdge.tail) { edge.unselect }
    }
    val filteredNotZeroEdge = setEdge.filter(e => e.isSelected && e.reduceCost != 0)
    for (edge <- filteredNotZeroEdge) { edge.unselect }
  }

  /**
   * Resorb the merge
   */
  def resorbMergeArbo = resorbMergeOf(this.inEdge, _.destination)
  def resorbMergeAnti = resorbMergeOf(this.outEdge, _.source)
  private def resorbMergeOf(setEdge: HashSet[Edge], srcOrDest: Edge => Vertice) = {
    val selected = setEdge.filter(_.isSelected).map(x => srcOrDest(x.prevLvlSelect.get))
    val t = this.prevLvl.get
    val goingnode = t.find(e => selected.contains(e)).get
    for {
      edge <- internal.get
      if edge.isSelected
      if (!(edge.reduceCost == 0 && srcOrDest(edge) != goingnode))
    } {
      edge.reject
    }
  }

  /**
   * For each neighbours still accessible
   * do some function
   */
  def forallSelectedNeighboursArbo(fct: Vertice => Unit) =
    forallSelectedNeighbours(fct, this.outEdge, _.destination)
  def forallSelectedNeighboursAnti(fct: Vertice => Unit) =
    forallSelectedNeighbours(fct, this.inEdge, _.source)
  private def forallSelectedNeighbours(fct: Vertice => Unit, setEdge: HashSet[Edge], srcOrDest: Edge => Vertice) = {
    for {
      edge <- setEdge
      if (edge.isSelected)
    } {
      fct(srcOrDest(edge))
    }
  }

  override def toString: String = {
    "id:" + id + " lvl:" + level
  }

  def fullString: String = {
    var res = "id:" + id + " lvl:" + level + "\n"
    res += "inEdge  : " + inEdge.mkString("\n") + "\n"
    res += "outEdge : " + outEdge.mkString("\n") + "\n"
    res += "prevLvl : " + prevLvl + "\n"
    res += "internal: " + internal
    res
  }
}

object Vertice {
  def apply(id: Int, level: Int = 0) = new Vertice(id, level)()
}