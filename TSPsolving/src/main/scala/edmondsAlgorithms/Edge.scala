/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edmondsAlgorithms

import scala.collection.mutable.HashSet

class Edge(val source: Vertice,
  val destination: Vertice,
  val costInit: Double, val level: Int = 0)(var modifiedCost: Double = costInit, var reduceCost: Double = costInit, var isSelected: Boolean = true,
    var isRejected: Boolean = false) {

  var prevLvlSelect: Option[Edge] = None
  var prevLvlNot: Option[HashSet[Edge]] = None

  def cost = modifiedCost

  /**
   * Update the cost of the vertex
   */
  def updateCost =
    modifiedCost = costInit - source.modifyWeight - destination.modifyWeight

  /**
   * Reset the vertex
   */
  def reset = {
    this.reduceCost = this.modifiedCost
    this.isSelected = true
    this.isRejected = false
    this.prevLvlSelect = None
    this.prevLvlNot = None
  }

  /**
   * Totally reject edge
   * No more used in computation
   */
  def reject = {
    this.isSelected = false
    this.isRejected = true
  }

  /**
   * Unreject the edge
   */
  def unreject = {
    this.isSelected = true
    this.isRejected = false
  }

  /**
   * Unselect the edge
   * Still possible to go back
   */
  def unselect = {
    this.isSelected = false
  }

  /**
   * If not totally discarded
   * select the edge
   */
  def enable = {
    if (!isSelected && !isRejected)
      this.isSelected = true
  }

  /**
   * Reduce the cost of the value
   */
  def reduceCost(value: Double): Unit =
    this.reduceCost = this.reduceCost - value

  override def toString: String = {
    "s:" + source.id + " d:" + destination.id + " c:" + cost + " m:" + reduceCost + " lvl:" + level + " selected:" + isSelected + " rejected:" + isRejected
  }

  def toTriplet = (source.id, destination.id, cost)
  def toMapElem = ((source.id, destination.id), cost)
}

object Edge {
  def apply(source: Vertice, destination: Vertice, cost: Double, level: Int = 0) = {
    val newedge = new Edge(source, destination, cost, level)()
    source.addOut(newedge)
    destination.addIn(newedge)
    newedge
  }
}