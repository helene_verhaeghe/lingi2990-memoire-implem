/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edmondsAlgorithms

import scala.collection.mutable.Map
import scala.collection.mutable.Stack
import scala.collection.mutable.HashSet

import graph.GraphCollection
import heldKarpSymmetric._
import graph.TimeSaver
import myVisu._
import tsplib._
import heldKarpSymmetric.WeightedFunction

class Graph(val verticeMap: Map[Int, Map[Int, Vertice]]) {

  private var level: Int = 0

  /**
   * Retrieve the map of vertice for the lvl
   */
  def thisLvlVerticeMap = this.verticeMap(level)

  /**
   * Give the vertice of the current lvl
   */
  def vertice(node: Int): Vertice = {
    thisLvlVerticeMap(node)
  }

  /**
   * Apply fct to all the vertice of the current lvl
   */
  def foreachVerticeOfLvlDo(fct: Vertice => Unit) = {
    for ((_, vertice) <- thisLvlVerticeMap) { fct(vertice) }
  }

  /**
   * Search for a cycle and return the first found.
   * If no cycle found, should be an arbo
   */
  def findCycleArbo: Option[HashSet[Vertice]] =
    findCycle((v, fct) => v.forallSelectedNeighboursArbo(fct))
  def findCycleAnti: Option[HashSet[Vertice]] =
    findCycle((v, fct) => v.forallSelectedNeighboursAnti(fct))
  private def findCycle(forSelectedNeighbours: (Vertice, (Vertice => Unit)) => Unit) = {
    val visited: HashSet[Vertice] = HashSet()
    var cycle: Option[HashSet[Vertice]] = None

    foreachVerticeOfLvlDo { vertice =>
      if (!visited.contains(vertice) && cycle.isEmpty) {
        val stack = Stack(vertice)
        val visitNow = HashSet(vertice)
        val prev: Map[Vertice, Vertice] = Map() // (elem, previous of the element)
        while (!stack.isEmpty && cycle.isEmpty) {
          val firstelem = stack.pop
          forSelectedNeighbours(firstelem, {
            neighbour =>
              if ((cycle.isEmpty) && (!visited.contains(neighbour))) {
                if (visitNow.contains(neighbour)) {
                  cycle = Some(HashSet(neighbour))
                  var previous = firstelem
                  while (previous != neighbour) {
                    cycle.get += previous
                    previous = prev(previous)
                  }
                } else {
                  visitNow += neighbour
                  stack.push(neighbour)
                  prev += ((neighbour, firstelem))
                }
              }
          })
        }
        visited ++= visitNow
      }
    }
    cycle
  }

  /**
   * Merge the cycle contained in the hashset
   */
  def merge(toMerge: HashSet[Vertice]) = {
    val verticeMap = thisLvlVerticeMap
    val internals: HashSet[Edge] = HashSet()
    val out: HashSet[Edge] = HashSet()
    val in: HashSet[Edge] = HashSet()
    // find all edges involved in the cycle
    for (vertice <- toMerge) {
      for {
        edge <- vertice.inEdge
        if (!edge.isRejected)
      } {
        if (toMerge.contains(edge.source))
          internals += edge
        else
          in += edge
      }
      for {
        edge <- vertice.outEdge
        if (!edge.isRejected)
        if (!toMerge.contains(edge.destination))
      } {
        out += edge
      }
    }

    val newvertice = Vertice(toMerge.minBy(_.id).id, this.level + 1)
    newvertice.prevLvl = Some(toMerge)
    newvertice.internal = Some(internals)

    val ingroup = in.groupBy(_.source)
    for {
      (source, set) <- ingroup
    } {
      val selected = set.minBy(_.reduceCost)
      val newedge = Edge(source, newvertice, selected.reduceCost, this.level + 1)
      for (edge <- set) { edge.source.outEdge -= edge }
      newedge.prevLvlSelect = Some(selected)
      selected.unselect
      set -= selected
      newedge.prevLvlNot = Some(set)
      set.foreach(_.reject)
    }

    val outgroup = out.groupBy(_.destination)
    for {
      (destination, set) <- outgroup
    } {
      val selected = set.minBy(_.reduceCost)
      val newedge = Edge(newvertice, destination, selected.reduceCost, this.level + 1)
      for (edge <- set) { edge.destination.inEdge -= edge }
      newedge.prevLvlSelect = Some(selected)
      selected.unselect
      set -= selected
      newedge.prevLvlNot = Some(set)
      set.foreach(_.reject)
    }

    this.level += 1
    val newmap = verticeMap.filter(e => !toMerge.contains(e._2))
    newmap += ((newvertice.id, newvertice))
    this.verticeMap += ((this.level, newmap))
    newvertice
  }

  /**
   * Unmerge the wanted vertice
   */
  def unmergeArbo(toUnmerge: Vertice) = unmerge(toUnmerge, _.resorbMergeArbo)
  def unmergeAnti(toUnmerge: Vertice) = unmerge(toUnmerge, _.resorbMergeAnti)
  private def unmerge(toUnmerge: Vertice, resorbMerge: Vertice => Unit) = {
    if (toUnmerge.level == this.level) {
      val verticeMap = thisLvlVerticeMap
      this.verticeMap.remove(this.level)
      resorbMerge(toUnmerge)
      for (edge <- toUnmerge.inEdge) {
        val prev = edge.prevLvlSelect.get
        prev.enable
        edge.source.outEdge -= edge
        prev.source.outEdge += prev
        prev.reduceCost = edge.reduceCost
        prev.isSelected = edge.isSelected
        for {
          opt <- edge.prevLvlNot
          edge <- opt
        } {
          edge.source.outEdge += edge
        }
      }
      for (edge <- toUnmerge.outEdge) {
        val prev = edge.prevLvlSelect.get
        prev.enable
        edge.destination.inEdge -= edge
        prev.destination.inEdge += prev
        prev.reduceCost = edge.reduceCost
        prev.isSelected = edge.isSelected
        for {
          opt <- edge.prevLvlNot
          edge <- opt
        } {
          edge.destination.inEdge += edge
        }
      }
      this.level -= 1
    }
  }

  /**
   * return true if an arbo/anti has been found
   * false if not
   */
  def findArbo(oneNode: Vertice): Boolean = {
    // STEP1 : reject all in edge of the root node
    oneNode.rejectInEdges
    // STEP2 : check if all vertice are connected to root node
    if (this.isConnectedToArbo(oneNode)) {
      // STEP3 : Find the arborescence
      this.findArboRootedIn(oneNode)
      true
    } else {
      false
    }
  }
  def findAnti(oneNode: Vertice): Boolean = {
    // STEP1 : reject all in edge of the root node
    oneNode.rejectOutEdges
    // STEP2 : check if all vertice are connected to root node
    if (this.isConnectedToAnti(oneNode)) {
      // STEP3 : Find the arborescence
      this.findAntiRootedIn(oneNode)
      true
    } else {
      false
    }
  }

  /**
   * If an arbo/anti is possible, return it
   */
  def findArboRootedIn(oneNode: Vertice): Unit = {
    // STEP1 : for each vertice, reduce cost of still selected in edge
    this.foreachVerticeOfLvlDo(_.reduceCostSelectedInEdge)
    // STEP2 : for each vertice, select only one in edge of 0 cost
    this.foreachVerticeOfLvlDo(_.selectOneInEdge)
    // STEP3 : check if this is an arborescence or not (in this case found cycle)
    val cycle = this.findCycleArbo
    if (cycle.isEmpty) {
      // STEP4 : definively reject all unselected edge
      // the selected edges forms the arborescence
      this.foreachVerticeOfLvlDo(_.rejectUnselectedInEdges)
    } else {
      // STEP4bis : merge the cycle, find arborescence, unmerge
      val mergeVertice = this.merge(cycle.get)
      this.findArboRootedIn(oneNode)
      this.unmergeArbo(mergeVertice)
    }

  }
  def findAntiRootedIn(oneNode: Vertice): Unit = {
    // STEP1 : for each vertice, reduce cost of still selected in edge
    this.foreachVerticeOfLvlDo(_.reduceCostSelectedOutEdge)
    // STEP2 : for each vertice, select only one in edge of 0 cost
    this.foreachVerticeOfLvlDo(_.selectOneOutEdge)
    // STEP3 : check if this is an arborescence or not (in this case found cycle)
    val cycle = this.findCycleAnti
    if (cycle.isEmpty) {
      // STEP4 : definively reject all unselected edge
      // the selected edges forms the arborescence
      this.foreachVerticeOfLvlDo(_.rejectUnselectedOutEdges)
    } else {
      // STEP4bis : merge the cycle, find arborescence, unmerge
      val mergeVertice = this.merge(cycle.get)
      this.findAntiRootedIn(oneNode)
      this.unmergeAnti(mergeVertice)
    }

  }

  /**
   * Check if all the vertices are connected to the one given
   * Check if an Arbo/Anti is possible
   */
  def isConnectedToArbo(oneNode: Vertice): Boolean =
    isConnectedToA(oneNode, (v, fct) => v.forallSelectedNeighboursArbo(fct))
  def isConnectedToAnti(oneNode: Vertice): Boolean =
    isConnectedToA(oneNode, (v, fct) => v.forallSelectedNeighboursAnti(fct))
  private def isConnectedToA(oneNode: Vertice, forSelectedNeighbours: (Vertice, (Vertice => Unit)) => Unit) = {
    val stack = Stack(oneNode)
    val visited = HashSet(oneNode)
    while (!stack.isEmpty) {
      val currentVertice = stack.pop
      forSelectedNeighbours(currentVertice, {
        vertice =>
          if (!visited.contains(vertice)) {
            stack push vertice
            visited += vertice
          }
      })
    }
    (visited.size == this.verticeMap(level).size)
  }

  /**
   * Find the 1Arbo/1Anti if there is one possible
   */
  def oneArborescence(oneNode: Int): Boolean = {
    val root = this.verticeMap(0)(oneNode)
    if (!(root.inEdge.size > 0)) return false
    val isThereArbo = this.findArbo(root)
    if (isThereArbo) {
      val minInEdge = root.inEdge.minBy(e => e.modifiedCost)
      minInEdge.unreject
    }
    return isThereArbo
  }
  def oneAntiArborescence(oneNode: Int): Boolean = {
    val root = this.verticeMap(0)(oneNode)
    if (!(root.outEdge.size > 0)) return false
    val isThereArbo = this.findAnti(root)
    if (isThereArbo) {
      val minOutEdge = root.outEdge.minBy(e => e.modifiedCost)
      minOutEdge.unreject
    }
    return isThereArbo
  }

  def costSelectedEdges = {
    var res = 0.0
    for {
      (id, vertice) <- thisLvlVerticeMap
      edge <- vertice.outEdge
      if (edge.isSelected)
    } {
      res += edge.cost
    }
    res
  }

  /**
   * Check the degree constraints
   */
  def isOneInAndOneOutEdgeSelected = {
    verticeMap(0).forall {
      case (id, v) =>
        (v.inEdge.count(e => e.isSelected) == 1) &&
          (v.outEdge.count(e => e.isSelected) == 1)
    }
  }

  /**
   * Update the cost of all edges with respect
   * to the weights of the vertices at the
   * beginning and the end of the edge
   */
  def updateCostOfAllEdge = {
    for {
      (id, vertice) <- verticeMap(0)
      edge <- vertice.inEdge
    } {
      edge.updateCost
    }
  }

  /**
   * Update the weight of all vertice wrt to
   * the number of selected edges linked to the vertice
   */
  def updateWeightOfAllVertice(beta: Double) = {
    for ((_, vertice) <- verticeMap(0)) {
      vertice.updateWeight(beta)
    }
  }

  /**
   * Reset level to 0 without modifying the modify costs of vertices
   * Remove higher map lvl
   * Reset edges and vertice
   */
  def unselectALL = {
    this.level = 0
    for {
      (lvl, map) <- verticeMap
      if lvl != 0
    } {
      verticeMap.remove(lvl)
    }
    for ((id, vertice) <- verticeMap(level)) {
      vertice.reset
      for (edge <- vertice.outEdge) { edge.reset }
    }
  }

  /**
   * Compute in the iterative way the addapted Held-Karp bound
   * Given a weight function, a one node to root the 1arbo/1anti,
   * a possible maximum number of iteration, a possible function
   * to proceed at each bound found
   */
  def bound(fct: WeightedFunction,
    oneNode: Int = 0,
    nbIter: Option[Int] = None,
    addPointToVisu: (Double, Double) => Unit = (x, y) => Unit,
    relaxTourArbo: Boolean = true,
    ub: Int = Int.MaxValue) = {

    unselectALL
    val relaxTour = if (relaxTourArbo)
      node => this.oneArborescence(node)
    else
      node => this.oneAntiArborescence(node)

    var iter = 0
    var bestsofar = 0.0

    val iterCheck = if (nbIter.isEmpty) { () => true } else { val nbIt = nbIter.get; () => nbIt > iter }
    val maxCheck = if (ub == Int.MaxValue) { () => true } else { () => bestsofar <= ub }
    while (iterCheck() && !isOneInAndOneOutEdgeSelected && maxCheck()) {
      val verticeMap = this.verticeMap(0)

      val beta = fct.genBeta(bestsofar, verticeMap.map(k => (2 - k._2.outEdge.count(e => e.isSelected) - k._2.inEdge.count(e => e.isSelected)) ^ 2).sum)

      updateWeightOfAllVertice(beta)
      this.updateCostOfAllEdge
      unselectALL
      relaxTour(oneNode)

      val bound = costSelectedEdges + verticeMap.map(_._2.modifyWeight).sum * 2
      bestsofar = bestsofar.max(bound)

      addPointToVisu(iter, bound)

      iter += 1
      fct.nextTime
    }
    bestsofar

  }

  /**
   * Compute all the 1Arbo/1Anti and list their value
   */
  def all1arbo = all1(this.oneArborescence(_))
  def all1antiarbo = all1(this.oneAntiArborescence(_))
  private def all1(onefct: Int => Boolean) = {
    var list: List[(Int, Double)] = List()
    val verticeMap = this.verticeMap(0)
    for {
      (hash, vertice) <- verticeMap
    } {
      unselectALL
      onefct(hash)
      val cost1Tree = costSelectedEdges + verticeMap.map(_._2.modifyWeight).sum * 2
      list = (hash, cost1Tree) +: list
    }
    unselectALL
    list
  }

  /**
   * Get the best 1Arbo/1Anti
   */
  def best(arbo: Boolean): Double = {
    if (arbo)
      best(i => this.oneArborescence(i))
    else
      best(i => this.oneAntiArborescence(i))
  }
  private def best(fct: Int => Unit = i => this.oneArborescence(i)): Double = {
    var bsf = 0.0
    var stop = false
    val verticeMap = this.verticeMap(0)
    for {
      (hash, vertice) <- verticeMap
      if (!stop)
    } {
      unselectALL
      fct(hash)
      val cost1Tree = costSelectedEdges + verticeMap.map(_._2.modifyWeight).sum * 2
      if (this.isOneInAndOneOutEdgeSelected) {
        bsf = cost1Tree
        stop = true
      } else {
        bsf = Math.max(bsf, cost1Tree)
      }
    }
    unselectALL
    bsf
  }

  override def toString: String = {
    var result = "Vertice : \n"
    for ((hash, vert) <- verticeMap(level).toList.sortBy(_._1)) {
      result += vert.toString + "\n"
      result += "     outEdge :\n"
      for (edge <- vert.outEdge) {
        result += "        " + edge + "\n"
      }
    }
    result
  }

  def fullString: String = {
    var result = "Vertice : \n"
    for ((hash, vert) <- thisLvlVerticeMap.toList.sortBy(_._1)) {
      result += vert.fullString
    }
    result
  }

  def arboString(oneNode: Vertice): String = {
    if (this.findCycleArbo.isEmpty) {
      val verticeMap = this.verticeMap(this.level)
      this.printneigbours(oneNode).mkString("\n")
    } else {
      "Not an arborescence"
    }
  }

  def printneigbours(vertice: Vertice): List[String] = {
    var res: List[String] = List(vertice.id.toString)
    for (vert <- vertice.outEdge.filter(_.isSelected).map(_.destination).toList.sortBy(i => i.id)) {
      for (v <- printneigbours(vert)) {
        res = ("  " + v) +: res
      }
    }
    res.reverse
  }
}

object Graph {

  def apply(vertice: Traversable[Int], edges: Traversable[(Int, Int, Int)]) = {
    val verticemap = Map() ++ vertice.map(id => (id, Vertice(id)))
    for ((s, d, cost) <- edges) {
      val source = verticemap(s)
      val destination = verticemap(d)
      val edge = Edge(source, destination, cost)
    }
    new Graph(Map((0, verticemap)))
  }

}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object test extends App {

  println("------------ init")
  //  val gf = GraphCollection.graph7.toEdmondsGraph
  val gf = TSPlib.br17.toGraph.toEdmondsGraph
  val root = gf.vertice(0)
  println(gf)
  println("------------ all")
  gf.findArbo(root)
  //  println(gf)
  println("------------ isArbo")
  println(gf.findCycleArbo)
  println("------------ arbo")
  println(gf.arboString(root))
  println("cost:" + gf.costSelectedEdges)

}

object testgraph extends App {
  println(Int.MinValue)

  val tspgraph = TSPlib.p43
  val graph = tspgraph.toGraph.getInvertedVersion

  //  val graph = GraphCollection.graph9

  val graph1 = graph.toEdmondsGraph

  val fct = () => new HeldKarpBound(tspgraph.optimal + 3000, graph.vertice.size)

  val nbit = 200

  val resultGraph = ResultGraph("Held Karp Asymetric of " + tspgraph.instance_name, "iteration", "bound", 0, nbit * 1.05)
  resultGraph.getHorizontalLine("optimal", tspgraph.optimal)
  val line_asym = resultGraph.getEvolutiveWithMax("asym")
  resultGraph.create

  println("---------------0 sym3")
  TimeSaver.setOffset
  val res1 = graph1.bound(fct(), 0, Some(nbit), line_asym.addTo) //, false)
  val t1 = TimeSaver.timePassed

}

object testgraphbis extends App {
  println(Int.MinValue)

  val tspgraph = TSPlib.ft53
  val graph = tspgraph.toGraph

  //  val graph = GraphCollection.graph9
  val graph0 = graph.toEdmondsGraph
  val graph1 = graph.toEdmondsGraph
  val graph2 = graph.toEdmondsGraph

  val fct = () => new HeldKarpBound(tspgraph.optimal, graph.vertice.size)

  val nbit = 200

  val resultGraph = ResultGraph("Held Karp Asymetric of " + tspgraph.instance_name, "iteration", "bound", 0, nbit * 1.05)
  resultGraph.getHorizontalLine("optimal", tspgraph.optimal)
  val line_asymmixe = resultGraph.getEvolutiveLine("asym mixe")
  val line_asym = resultGraph.getEvolutiveLine("asym arbo")
  val line_asymanti = resultGraph.getEvolutiveLine("asym antiarbo")
  resultGraph.create

  println("---------------0 sym3")
  TimeSaver.setOffset
  val res1 = graph1.bound(fct(), 0, Some(nbit), line_asym.addTo, true)
  val t1 = TimeSaver.timePassed
  TimeSaver.setOffset
  val res2 = graph2.bound(fct(), 0, Some(nbit), line_asymanti.addTo, false)
  val t2 = TimeSaver.timePassed

}

object TestHKgraphEdmond extends App {

  println(Int.MinValue)
  val nbit = 1000
  //  val tspgraph = TSPlib.ftv33
  //  val graph = tspgraph.toGraph
  val graph = GraphCollection.graph15
  val graph1 = graph.getTriSymmetricVersion
  val graph2 = graph.getSymmetricVersion

  val hkgraph1 = graph1.toHKgraph
  val hkgraph2 = graph2.toHKgraph
  val hkgraph1bis = graph1.toHKgraph
  val hkgraph2bis = graph2.toHKgraph
  val hkgraph2bisbis = graph.getSymmetricVersion(-1500).toHKgraph
  val graph0 = graph.toEdmondsGraph

  val fct = () => new HeldKarpBound(200, graph.vertice.size)

  val resultGraph = ResultGraph("Held Karp of " + graph.name, "iteration", "bound", 0, nbit * 1.05)
  //  resultGraph.getHorizontalLine("optimal", tspgraph.optimal)
  resultGraph.getHorizontalLine("optimal", 30)
  val line = (s: String) => resultGraph.getMaxOfEvolutive(s)
  val line_sym3 = line("sym3")
  val line_sym2 = line("sym2")
  val line_sym3force = line("sym3force")
  val line_sym2force = line("sym2force")
  val line_sym2biais = line("sym2biais")
  val line_asymHKbound = line("asym HKlam")
  val line_asym = line("asym 200/i")
  resultGraph.create

  println("---------------0 sym3")
  TimeSaver.setOffset
  val res1 = hkgraph1.bound(fct(), 0, Some(nbit), line_sym3.addTo)
  val t1 = TimeSaver.timePassed
  println("---------------1 sym2")
  TimeSaver.setOffset
  val res2 = hkgraph2.bound(fct(), 0, Some(nbit), line_sym2.addTo)
  val t2 = TimeSaver.timePassed
  println("---------------2 sym3 with force")
  TimeSaver.setOffset
  val res1bis = hkgraph1bis.bound(fct(), 0, Some(nbit), line_sym3force.addTo, true)
  val t3 = TimeSaver.timePassed
  println("---------------3 sym2 with force")
  TimeSaver.setOffset
  val res2bis = hkgraph2bis.bound(fct(), 0, Some(nbit), line_sym2force.addTo, true)
  val t4 = TimeSaver.timePassed
  println("---------------4 sym2 with biais")
  TimeSaver.setOffset
  val res2bisbis = hkgraph2bisbis.bound(fct(), 0, Some(nbit), line_sym2biais.addTo, false, true)
  val t5 = TimeSaver.timePassed
  println("---------------5")
  TimeSaver.setOffset
  val res5 = graph0.bound(fct(), 0, Some(nbit), line_asymHKbound.addTo)
  val t6 = TimeSaver.timePassed

}

