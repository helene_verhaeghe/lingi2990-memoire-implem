/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package linearRelaxationTSP

import scala.collection.mutable.Map
import oscar.linprog.modeling._
import oscar.linprog._
import oscar.algebra._
import graph._

/**
 * Compute the linear relaxation lower bound of asymmetric TSP
 *
 * Algorithm of p43 of the paper :
 *   Analysis of the Held-Karp Heuristic for the Traveling
 *   Salesman Problem by David Paul Williamson
 *
 * Same lower bound of the Held-Karp Heuristic
 * is the case of an asymmetric TSP
 *
 * 			minimize  	sum (c_ij x_ij) {with 1<=i<j<=n}
 *    		subject to	sum (x_ij) {for i} = 1   					for j=1,2,...,n
 *      				sum (x_ij) {for j} = 1   					for i=1,2,...,n
 *      				sum (x_ij) {with i in S, j in S} <= |S|-1   for each proper subset S\in V
 *             			x_ij >= 0 									1<=i<j<=n
 */
class LinearRelaxationTSPasymmetric extends LPModel(LPSolverLib.lp_solve) {

  def asymmetricTSP(graph: AbstractGraph): SubGraph = {

    TimeSaver.setOffset

    // cst 4
    def nvar(name: String) = LPFloatVar(this.lpsolver, name, 0)

    val roads = RoadMaker(graph.edgeToMapByPairs)(nvar)
    val cities = graph.listVertice

    // cst 1 & 2
    for {
      city <- cities
    } {
      val filteredRoadsOutgoing = FilterRoadsBy(roads, (i, j) => (i == city))
      add(sum(filteredRoadsOutgoing) { _.x } == 1)

      val filteredRoadsIncomming = FilterRoadsBy(roads, (i, j) => (j == city))
      add(sum(filteredRoadsIncomming) { _.x } == 1)
    }

    // cst 3
    for {
      citysubset <- GetSubsets(cities)
    } {
      val filteredRoadsContained = FilterRoadsBy(roads, (i, j) => (citysubset.contains(i) && citysubset.contains(j)))
      add(sum(filteredRoadsContained) { _.x } <= citysubset.size - 1)
    }

    minimize(sum(roads) { case (_, road) => road.c * road.x })

    start()

    val value = objectiveValue
    val assign = roads.map { case ((i, j), road) => ((i, j), road.x.value) }
    val solution = SubGraph(graph, value, assign, ImplemLinearRelaxationTSPasymmetric, Some(TimeSaver.timePassed))

    release()

    solution
  }

}

object LinearRelaxationTSPasymmetric {
  def apply(graph: AbstractGraph) = {
    val LP = new LinearRelaxationTSPasymmetric
    LP.asymmetricTSP(graph)
  }
}

object ImplemLinearRelaxationTSPasymmetric extends Implem {
  def asString: String = "Linear Relaxation for Asymmetric TSP"
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object TestLinearRelaxationTSPasymmetric extends App {
  println(LinearRelaxationTSPasymmetric(GraphCollection.graph13))
}
