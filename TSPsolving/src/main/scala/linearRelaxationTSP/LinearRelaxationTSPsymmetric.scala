/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package linearRelaxationTSP

import scala.collection.mutable.Map
import oscar.linprog.modeling._
import oscar.linprog._
import oscar.algebra._
import graph._

/**
 * Compute the linear relaxation lower bound of symmetric TSP
 *
 * Algorithm of theorem 1.2.1 of p16 of the paper :
 *   Analysis of the Held-Karp Heuristic for the Traveling
 *   Salesman Problem by David Paul Williamson
 *
 * Same lower bound of the Held-Karp Heuristic
 * is the case of a symmetric TSP
 *
 * 			minimize  	sum (c_ij x_ij) {with 1<=i<j<=n}
 *    		subject to	sum (x_ij) {with j>i} + sum (x_ij) {with i>j} = 2   for i=1,2,...,n
 *      				sum (x_ij) {with i in S, j in S, i<j} <= |S|-1   for each proper subset S\in V
 *          			x_ij <= 1											1<=i<j<=n
 *             			x_ij >= 0 											1<=i<j<=n
 *
 * Graph must be declared with i<j already
 */
class LinearRelaxationTSPsymmetric extends LPModel(LPSolverLib.lp_solve) {

  def symmetricTSP(graph: AbstractSymmetricGraph, mustForceLinkEdges: Boolean = false): SubGraph = {

    TimeSaver.setOffset

    // cst 3 and 4
    def nvar(name: String) = LPFloatVar(name, 0, 1)

    val roads = RoadMaker(graph.edgeToMapByPairs)(nvar)
    val cities = graph.listVertice

    // cst 1
    for {
      city <- cities
    } {
      val filteredRoadsIncident = FilterRoadsBy(roads, (i, j) => (i == city || j == city))
      add(sum(filteredRoadsIncident)(_.x) == 2)
    }

    // cst 2
    for {
      citysubset <- GetSubsets(cities)
    } {
      val filteredRoadsContained = FilterRoadsBy(roads, (i, j) => (citysubset.contains(i) && citysubset.contains(j)))
      add(sum(filteredRoadsContained) { _.x } <= citysubset.size - 1)
    }

    // force linking edges
    if (mustForceLinkEdges) {
      for {
        (i, j) <- graph.linkingEdges
      } {
        add(roads((i, j)).x == 1)
      }
    }

    minimize(sum(roads) { case (_, road) => road.c * road.x })

    start() // TODO : Silent this thing

    val value = objectiveValue.map(_ + graph.biais)
    val assign = roads.map { case ((i, j), road) => ((i, j), road.x.value) }
    val solution = SubGraph(graph, value, assign, if (mustForceLinkEdges) ImplemLinearRelaxationTSPsymmetricWithForce else ImplemLinearRelaxationTSPsymmetric, Some(TimeSaver.timePassed))

    release()

    solution
  }

}

object LinearRelaxationTSPsymmetric {
  def apply(graph: AbstractSymmetricGraph) = {
    val LP = new LinearRelaxationTSPsymmetric
    LP.symmetricTSP(graph)
  }
}

object ImplemLinearRelaxationTSPsymmetric extends Implem {
  def asString: String = "Linear Relaxation for Symmetric TSP"
}

object LinearRelaxationTSPsymmetricWithForce {
  def apply(graph: AbstractSymmetricGraph) = {
    val LP = new LinearRelaxationTSPsymmetric
    LP.symmetricTSP(graph, true)
  }
}

object ImplemLinearRelaxationTSPsymmetricWithForce extends Implem {
  def asString: String = "Linear Relaxation for Symmetric TSP with forcing of the linking edges"
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object TestLinearRelaxationTSPsymmetric extends App {
  println(LinearRelaxationTSPsymmetric(GraphCollection.graph13.getTriSymmetricVersion))
}
