/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package linearRelaxationTSP

import scala.collection.mutable.Map
import oscar.linprog.modeling._
import oscar.linprog._
import oscar.algebra._

/**
 * Classes to handle Road object for the linear relaxations
 */
object RoadMaker {
  def apply(map: Map[(Int, Int), Int])(nvar: String => LPFloatVar): Map[(Int, Int), Roadij] = {
    for {
      ((i, j), c) <- map
    } yield {
      ((i, j), Roadij(nvar("x_" + i + "_" + j), i, j, c))
    }
  }
}

case class Roadij(x: LPFloatVar, i: Int, j: Int, c: Int)

object FilterRoadsBy {
  def apply(roads: Map[(Int, Int), Roadij], filter: (Int, Int) => Boolean): Iterable[Roadij] = {
    for {
      ((i, j), road) <- roads
      if (filter(i, j))
    } yield { road }
  }
}

object GetSubsets {
  def apply(list: List[Int]) = list.toSet.subsets.filter(p => p.size >= 2 && p.size < list.size)
}