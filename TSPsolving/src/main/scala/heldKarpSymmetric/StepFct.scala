/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heldKarpSymmetric

/**
 * Function to generate the value of beta following the iteration
 */
abstract class WeightedFunction {

  var t: Int = 0

  def genBeta(bsf: => Double, diff: => Double): Double = {
    if (t == 0) 0 else gen(bsf, diff)
  }

  /**
   * Function to compute the beta in function of t
   * Diff = sum_v ((2- sum x_in/out)^2)
   */
  def gen(bsf: => Double, diff: => Double): Double

  def nextTime = { t += 1 }

}

/**
 * Complex step size with update
 */
case class HeldKarpBound(val ub: Double, val size: Int) extends WeightedFunction {

  var limit = size
  var k = 0
  val r = (size / 100.0).ceil.toInt
  val lambda = 0.5

  def gen(bsf: => Double, diff: => Double): Double = {
    if (t < limit)
      (lambda / Math.pow(2, k)) * (ub - bsf) / diff
    else {
      k += 1
      if (limit > r)
        limit += (2 * size / Math.pow(2, k)).toInt
      else
        limit += r
      (lambda / Math.pow(2, k)) * (ub - bsf) / diff
    }
  }
}

/**
 * Complex step size
 */
case class HeldKarpBoundFixed(val ub: Double, val lambda: Double) extends WeightedFunction {
  def gen(bsf: => Double, diff: => Double): Double = {
    lambda * (ub - bsf) / diff
  }
}

/**
 * Simple step size
 */
case class OverK(val base: Double = 200.0) extends WeightedFunction {
  def gen(bsf: => Double, diff: => Double): Double = {
    base / t
  }
}

/**
 * Geometric step size
 */
case class Geometric(val base: Double = 200.0, val power: Double = 0.9999) extends WeightedFunction {
  def gen(bsf: => Double, diff: => Double): Double = {
    base * Math.pow(power, t - 1)
  }
}

