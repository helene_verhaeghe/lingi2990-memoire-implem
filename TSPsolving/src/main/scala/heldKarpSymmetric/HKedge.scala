/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heldKarpSymmetric

/**
 * Structure of edge used for the Held-Karp
 * iterative bounding procedure based on the
 * 1Tree
 */
class HKedge(val source: HKvertice,
  val destination: HKvertice,
  val cost: Int)(var modifiedCost: Double = cost, var isSelected: Boolean = false) {

  def updateCost = {
    modifiedCost = cost - source.modifyWeight - destination.modifyWeight
  }

  def select = { isSelected = true }

  def unselect = { isSelected = false }

  def toTriplet = (source.id, destination.id, modifiedCost)

  override def toString = "(" + source.id + "," + destination.id + "," + cost + "-" + modifiedCost + ")"
}

object HKedge {
  def apply(source: HKvertice, destination: HKvertice, cost: Int) = {
    val newedge = new HKedge(source, destination, cost)()
    source.add(newedge)
    destination.add(newedge)
    newedge
  }
}
