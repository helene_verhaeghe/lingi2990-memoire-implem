/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heldKarpSymmetric

import scala.collection.mutable.HashSet

/**
 * Structure of vertex used for the Held-Karp
 * iterative bounding procedure based on the
 * 1Tree
 */
class HKvertice(val id: Int)(var modifyWeight: Double = 0.0) {
  val edge: HashSet[HKedge] = HashSet()

  def add(edge: HKedge) = { this.edge += edge }

  /**
   * Update the weight of the vertex
   * with respect to the beta
   */
  def updateWeight(beta: Double) = {
    val degree = edge.count(e => e.isSelected)
    modifyWeight += (2 - degree) * beta
  }
}

object HKvertice {
  def apply(id: Int) = {
    new HKvertice(id)()
  }
}