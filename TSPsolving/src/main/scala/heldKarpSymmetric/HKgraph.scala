/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heldKarpSymmetric

import scala.collection.mutable.Map
import scala.collection.mutable.Set
import graph._
import oscar.algo.DisjointSets

/**
 * Structure of graph used for the Held-Karp
 * iterative bounding procedure based on the
 * 1Tree
 */
class HKgraph(val graph: AbstractSymmetricGraph, val verticeMap: Map[Int, HKvertice], val edgeSet: Set[HKedge]) {

  val edgeMap = Map() ++ edgeSet.map(e => ((e.source.id, e.destination.id), e))

  /**
   * Unselect all the edges
   */
  def unselectALL =
    for (edge <- edgeSet) { edge.unselect }

  /**
   * Update the cost of all edges with respect
   * to the weights of the vertices at the
   * beginning and the end of the edge
   */
  def updateCostALL =
    for (edge <- edgeSet) { edge.updateCost }

  /**
   * Update the weight of all vertice wrt to
   * the number of selected edges linked to the vertice
   */
  def updateWeightALL(beta: Double) =
    for ((_, vertice) <- verticeMap) { vertice.updateWeight(beta) }

  /**
   * Select the edge forming the one tree
   * with the min cost
   * (implem based on Disjoint Sets)
   */
  def oneTree(oneNode: Int, mustForceLinkEdges: Boolean = false) = {
    val ds = new DisjointSets(0, verticeMap.size - 1)
    var selected = 0
    var selectedOne = 0
    if (mustForceLinkEdges) {
      for ((s, d) <- graph.linkingEdges) {
        if (edgeMap.contains((s, d)))
          edgeMap((s, d)).select
        else
          edgeMap((d, s)).select
        if (s != oneNode && d != oneNode) {
          ds.union(s, d)
          selected += 1
        } else {
          selectedOne += 1
        }
      }
    }
    val sortedEdges = edgeSet.toArray.map(e => (e.toTriplet, e)).sortBy(_._1._3)
    for {
      ((s, d, c), e) <- sortedEdges
      if (selected + selectedOne < verticeMap.size)
      if (!e.isSelected)
    } {
      if (s != oneNode && d != oneNode) {
        if (selected < verticeMap.size - 2 && !ds.inSameSet(s, d)) {
          e.select
          ds.union(s, d)
          selected += 1
        }
      } else {
        if (selectedOne < 2) {
          e.select
          selectedOne += 1
        }
      }
    }

  }

  /**
   * Verify that the degree constraint is verified
   */
  def check2Selected =
    verticeMap.forall { case (id, v) => v.edge.count(e => e.isSelected) == 2 }

  /**
   * Compute the cost
   */
  def cost = {
    var sum = 0.0
    for {
      (id, v) <- verticeMap
      e <- v.edge
      if (e.source == v) && e.isSelected
    } {
      sum += e.modifiedCost
    }
    sum
  }

  /**
   * Compute in the iterative way the Held-Karp bound
   * Given a weight function, a one node to root the tree,
   * a possible maximum number of iteration, a possible function
   * to proceed at each bound found
   */
  def bound(fct: WeightedFunction, oneNode: Int = 0, nbIter: Option[Int] = None, addPointToVisu: (Double, Double) => Unit = (x, y) => Unit,
    mustForceLinkEdges: Boolean = false, biased: Boolean = false, ub: Int = Int.MaxValue) = {
    unselectALL
    var boundEvol: List[Double] = List()
    var boundEvolBis: List[(Double, Long)] = List()
    var iter = 0
    var bestsofar = 0.0

    var bound = 0.0
    //iteration
    val iterCheck = if (nbIter.isEmpty) { () => true } else { val nbIt = nbIter.get; () => nbIt > iter }
    val maxCheck = if (ub == Int.MaxValue) { () => true } else { () => bestsofar <= ub }
    while (iterCheck() && !check2Selected && maxCheck()) {
      val beta = fct.genBeta(bestsofar, 1.0 * verticeMap.map(k => (2 - k._2.edge.count(e => e.isSelected)) ^ 2).sum)
      updateWeightALL(beta)
      updateCostALL
      unselectALL
      oneTree(oneNode, mustForceLinkEdges)
      bound = if (biased) {
        cost + 2 * verticeMap.map(_._2.modifyWeight).sum + graph.biais
      } else
        cost + 2 * verticeMap.map(_._2.modifyWeight).sum
      bestsofar = bestsofar.max(bound)

      addPointToVisu(iter, bound)
      iter += 1
      fct.nextTime
    }
    bestsofar

  }

  /**
   * Compute all the 1Tree and list their respective costs
   */
  def all1Tree(biased: Boolean = false, mustForceLinkEdges: Boolean = false) = {
    var list: List[(Int, Double)] = List()
    for ((hash, vertice) <- verticeMap) {
      unselectALL
      oneTree(hash, mustForceLinkEdges)
      val cost1Tree = cost + (if (biased) { graph.biais } else { 0 })
      list = (hash, cost1Tree) +: list
    }
    unselectALL
    list
  }

  override def toString = {
    var result = ""
    for ((hash, vert) <- verticeMap) {
      result += "vertice " + hash + " (" + vert.modifyWeight + ")\n"
      for {
        edge <- vert.edge
        if edge.source == vert
      } {
        result += "   " + edge.toString
        if (edge.isSelected)
          result += " selected"
        result += "\n"
      }
    }
    result
  }
}

/**
 * Companion object
 */
object HKgraph {
  def apply(graph: AbstractSymmetricGraph) = {
    val verticemap = Map() ++ graph.vertice.map(id => (id, HKvertice(id)))
    var edgeSet: Set[HKedge] = Set()
    for ((s, d, cost) <- graph.edge) {
      val source = verticemap(s)
      val destination = verticemap(d)
      val edge = HKedge(source, destination, cost)
      edgeSet += edge
    }
    new HKgraph(graph, verticemap, edgeSet)
  }
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */
object TestHKgraph extends App {
  import tsplib._
  import myVisu._

  println(Int.MinValue)
  val nbit = 500
  val tspgraph = TSPlib.br17
  //val graph = GraphCollection.graph1
  val graph = tspgraph.toGraph
  val graph1 = graph.getTriSymmetricVersion
  val graph2 = graph.getSymmetricVersion

  val hkgraph1 = graph1.toHKgraph
  val hkgraph2 = graph2.toHKgraph
  val hkgraph1bis = graph1.toHKgraph
  val hkgraph2bis = graph2.toHKgraph
  val hkgraph2bisbis = graph.getSymmetricVersion(-1500).toHKgraph

  val fct = () => new Geometric(5, 0.9)

  val resultGraph = ResultGraph("Held Karp of " + tspgraph.instance_name, "iteration", "bound", 0, nbit * 1.05)
  resultGraph.getHorizontalLine("optimal", tspgraph.optimal)
  val line = (s: String) => resultGraph.getMaxOfEvolutive(s)
  val line_sym3 = line("sym3")
  val line_sym2 = line("sym2")
  val line_sym3force = line("sym3force")
  val line_sym2force = line("sym2force")
  val line_sym2biais = line("sym2biais")
  resultGraph.create

  println("---------------0 sym3")
  TimeSaver.setOffset
  val res1 = hkgraph1.bound(fct(), 0, Some(nbit), line_sym3.addTo)
  val t1 = TimeSaver.timePassed
  println("---------------1 sym2")
  TimeSaver.setOffset
  val res2 = hkgraph2.bound(fct(), 0, Some(nbit), line_sym2.addTo)
  val t2 = TimeSaver.timePassed
  println("---------------2 sym3 with force")
  TimeSaver.setOffset
  val res1bis = hkgraph1bis.bound(fct(), 0, Some(nbit), line_sym3force.addTo, true)
  val t3 = TimeSaver.timePassed
  println("---------------3 sym2 with force")
  TimeSaver.setOffset
  val res2bis = hkgraph2bis.bound(fct(), 0, Some(nbit), line_sym2force.addTo, true)
  val t4 = TimeSaver.timePassed
  println("---------------4 sym2 with biais")
  TimeSaver.setOffset
  val res2bisbis = hkgraph2bisbis.bound(fct(), 0, Some(nbit), line_sym2biais.addTo, false, true)
  val t5 = TimeSaver.timePassed
  println("---------------5")

}
