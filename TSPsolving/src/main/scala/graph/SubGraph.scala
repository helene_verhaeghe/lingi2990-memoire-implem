/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package graph

import scala.collection.mutable.Map

/**
 * Subgraph implem to keep track of the result
 * of the Branch & bound, the asymmetric linear
 * relaxation or the symmetric linear relaxation
 */
class SubGraph(
  val graph: AbstractGraph,
  val value: Option[Double],
  val assign: Map[(Int, Int), Option[Double]],
  val from: Implem,
  val time: Option[Long] = None) {

  def shortString: String = {
    "Solution of " + from + " of graph \n" +
      "    \"" + graph.name + "\"\n" +
      "value = " + value +
      (if (time.isEmpty) { "" }
      else {
        "\n" + "time  = " + TimeSaver.string(time.get)
      })
  }

  override def toString: String = {
    this.shortString + "\n" +
      "with assignation : \n" +
      assign.toList.sortBy(_._1).map { case ((i, j), value) => "    ( " + i + ", " + j + " ) = " + value }.mkString("\n")
  }

}

object SubGraph {
  def apply(graph: AbstractGraph, value: Option[Double], assign: Map[(Int, Int), Option[Double]], from: Implem, time: Option[Long] = None): SubGraph = {
    new SubGraph(graph, value, assign, from, time)
  }
}

trait Implem {
  def asString: String
  override def toString: String = asString
}

class SubGraphTSP(
  val graph: AbstractGraph,
  val value: Option[Double],
  val assign: Map[(Int, Int), Option[Int]],
  val from: Implem,
  val time: Option[Long] = None) {

  def shortString: String = {
    "Solution of " + from + " of graph \n" +
      "    \"" + graph.name + "\"\n" +
      "value = " + value +
      (if (time.isEmpty) { "" }
      else {
        "\n" + "time  = " + TimeSaver.string(time.get)
      })
  }

  override def toString: String = {
    this.shortString + "\n" +
      "with assignation : \n" +
      assign.toList.sortBy(_._1).map { case ((i, j), value) => "    ( " + i + ", " + j + " ) = " + value }.mkString("\n")
  }

}

object SubGraphTSP {
  def apply(graph: AbstractGraph, value: Option[Double], assign: Map[(Int, Int), Option[Int]], from: Implem, time: Option[Long] = None): SubGraphTSP = {
    new SubGraphTSP(graph, value, assign, from, time)
  }
}