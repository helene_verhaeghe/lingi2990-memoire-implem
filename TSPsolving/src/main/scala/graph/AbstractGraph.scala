/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package graph

import scala.collection.mutable.Set
import scala.collection.mutable.Map
import edmondsAlgorithms._
import heldKarpSymmetric._
import graph._

/**
 * Class to keep general graph data's
 */
class AbstractGraph(
  val vertice: Traversable[Int],
  val edge: Traversable[(Int, Int, Int)],
  val name: String = "") {

  /**
   * Return true if the graph is symmetric, false else
   */
  def isSymmetric = false

  def edgeToMapByPairs: Map[(Int, Int), Int] = {
    Map() ++ edge.map { case (i, j, c) => ((i, j), c) }
  }

  def listVertice = vertice.toList

  def ub: Int = {
    val ub1 = edge.groupBy(_._1).map(x => x._2.maxBy(_._3)._3).sum
    val ub2 = edge.groupBy(_._2).map(x => x._2.maxBy(_._3)._3).sum
    Math.min(ub1, ub2)
  }
  /**
   *  ====================
   *  Usable graph methods
   *  ====================
   */

  /**
   * Create an edmond graph structure
   */
  def toEdmondsGraph(root: Int): Graph = {
    Graph(vertice, edge)
  }

  def toEdmondsGraph: Graph = toEdmondsGraph(0)

  /**
   * Create a HeldKarp graph structure
   */
  def toHKgraph: HKgraph = {
    if (!isSymmetric)
      println("WARNING : toHKgraph not used from symmetric")
    HKgraph(this.asInstanceOf[AbstractSymmetricGraph])
  }

  /**
   *  ======================
   *  Transformation methods
   *  ======================
   */

  /**
   * Create a double vertice symmetric version of the graph
   * node i transformed into node i, i+offset
   * node i serving as out gate, i+offset as in gate
   */
  def getSymmetricVersion(biais: Int = 0): AbstractSymmetricGraph = {
    val offset = vertice.max + 1
    var linkingEdges: List[(Int, Int)] = List()

    val newvertice = vertice ++
      vertice.map(_ + offset)
    val newedge = edge.map { case (s, d, c) => (d, s + offset, c) } ++
      vertice.map(s => { linkingEdges = linkingEdges :+ (s, s + offset); (s, s + offset, biais) })

    AbstractGraph.sym(newvertice, newedge, "sym of " + name + " biased by " + biais, linkingEdges, -biais * vertice.size)
  }

  def getSymmetricVersion: AbstractSymmetricGraph = getSymmetricVersion(0)

  /**
   * Create a triple vertice symmetric version of the graph
   * node i transformed into node i, i+offset1, i+offset2
   * node i serving as out gate, i+offset1 as in gate and i+offset2 as center
   */
  def getTriSymmetricVersion: AbstractSymmetricGraph = {
    val offset1 = vertice.max + 1
    val offset2 = 2 * offset1
    val mincost = 0
    var linkingEdges: List[(Int, Int)] = List()

    val newvertice = vertice ++
      vertice.map(_ + offset1) ++
      vertice.map(_ + offset2)
    val newedge = edge.map { case (s, d, c) => (d, s + offset1, c) } ++
      vertice.map(s => { linkingEdges = linkingEdges :+ (s, s + offset2); (s, s + offset2, mincost) }) ++
      vertice.map(s => { linkingEdges = linkingEdges :+ (s + offset1, s + offset2); (s + offset1, s + offset2, mincost) })

    AbstractGraph.sym(newvertice, newedge, "trisym of " + name, linkingEdges)
  }

  def getInvertedVersion: AbstractGraph = {
    AbstractGraph(vertice, edge.map(x => (x._2, x._1, x._3)), "invert of " + name)
  }

  def getUnderlyingSymmetricVersion: AbstractSymmetricGraph = {
    val newedge = edge.map { case (i, j, c) => if (i > j) { (j, i, c) } else { (i, j, c) } }
    AbstractGraph.sym(vertice, edge, "undersym of " + name)
  }

  /**
   * Compute the ratios
   * mean mean in / mean mean out
   * mean max in / mean max out
   * mean min in / mean min out
   * mean var in / mean var out
   * mean deg in / mean deg out
   */
  def ratios: Array[Double] = { // mean, max, min, var,degree
    val size = vertice.size
    val incost = vertice.toArray.map(i => edge.filter(p => p._2 == i).map(p => p._3))
    val outcost = vertice.toArray.map(i => edge.filter(p => p._1 == i).map(p => p._3))
    val incostdata = incost.map { t => val mean = 1.0 * t.sum / t.size; (t.min, t.max, mean, 1.0 * t.map(k => (k - mean) * (k - mean)).sum / t.size) }
    val outcostdata = outcost.map { t => val mean = 1.0 * t.sum / t.size; (t.min, t.max, mean, 1.0 * t.map(k => (k - mean) * (k - mean)).sum / t.size) }
    val incostsum = incostdata.foldLeft((0, 0, 0.0, 0.0))((a: (Int, Int, Double, Double), b: (Int, Int, Double, Double)) => (a._1 + b._1, a._2 + b._2, a._3 + b._3, a._4 + b._4))
    val outcostsum = outcostdata.foldLeft((0, 0, 0.0, 0.0))((a: (Int, Int, Double, Double), b: (Int, Int, Double, Double)) => (a._1 + b._1, a._2 + b._2, a._3 + b._3, a._4 + b._4))
    val degree = incost.map(_.size).zip(outcost.map(_.size)).map(a => if (a._1 > a._2) 2 else (if (a._1 == a._2) 1 else 0))
    val degreeratio = if (degree.forall(_ == 1)) -1.0
    else {
      val in = degree.count(_ > 1)
      val out = degree.count(_ < 1)
      if (in > out) 2.0 else (if (in < out) 0.0 else 1.0)
    }
    val res = Array(1.0 * incostsum._1 / outcostsum._1, 1.0 * incostsum._2 / outcostsum._2, incostsum._3 / outcostsum._3, incostsum._4 / outcostsum._4, degreeratio)
    res
  }

  /**
   * Predict with the mean of the variance if
   * it will be likely to have a better result
   * with the 1Arbo or with the 1Anti
   */
  def predictorBetterUseArbo: Boolean = {
    val size = vertice.size
    val incost = vertice.toArray.map(i => edge.filter(p => p._2 == i).map(p => p._3))
    val outcost = vertice.toArray.map(i => edge.filter(p => p._1 == i).map(p => p._3))
    val incostdata = incost.map { t => val mean = 1.0 * t.sum / t.size; 1.0 * t.map(k => (k - mean) * (k - mean)).sum / t.size }
    val outcostdata = outcost.map { t => val mean = 1.0 * t.sum / t.size; 1.0 * t.map(k => (k - mean) * (k - mean)).sum / t.size }
    val incostsum = incostdata.sum
    val outcostsum = outcostdata.sum

    (incostsum <= outcostsum)
  }

  /**
   *  ================
   *  Printing methods
   *  ================
   */

  /**
   * Standard print
   */
  override def toString: String = {
    (if (isSymmetric) "Symmetric" else "Asymmetric") + " graph " + (if (name == "") "" else (name + " ")) + "with \n" +
      " - " + vertice.size + " vertices\n" +
      "    " + vertice.mkString(", ") + "\n" +
      " - " + edge.size + " edges\n" +
      "    " + edge.map { case (i, j, c) => "from " + i + " to " + j + " cost " + c }.mkString("\n    ")
  }

  /**
   * Print so it can be copy-past to reuse as graph directly
   */
  def toStringObject: String = {
    "AbstractGraph(\n" +
      "List(" + vertice.mkString(",") + "),\n" +
      "List(" + edge.mkString(",\n") + "),\n" +
      " \"" + name + "\",\n" +
      isSymmetric + ")"
  }

}

class AbstractSymmetricGraph(
  vertice: Traversable[Int],
  edge: Traversable[(Int, Int, Int)],
  name: String = "",
  val linkingEdges: List[(Int, Int)] = List(),
  val biais: Int = 0)
  extends AbstractGraph(vertice, edge, name) {

  val linkingEdgesMap = Map() ++ (linkingEdges ++ linkingEdges.map(e => (e._2, e._1))).groupBy(e => e._1)

  def getLinkedTo(vert: Int): List[Int] = {
    linkingEdgesMap(vert).map(_._2)
  }
  override def isSymmetric = true

  override def toEdmondsGraph(root: Int): Graph = {
    val newedge = edge ++ edge.map(e => (e._2, e._1, e._3))
    Graph(vertice, newedge)
  }

}

object AbstractGraph {

  def apply(vertice: Traversable[Int],
    edge: Traversable[(Int, Int, Int)],
    name: String = "",
    isSymmetric: Boolean = false): AbstractGraph = {

    if (isSymmetric) {
      AbstractGraph.sym(vertice, edge, name)
    } else {
      new AbstractGraph(vertice, edge, name)
    }
  }

  def sym(vertice: Traversable[Int],
    edge: Traversable[(Int, Int, Int)],
    name: String = "",
    linkingEdges: List[(Int, Int)] = List(),
    biais: Int = 0): AbstractSymmetricGraph = {

    val newedge = edge.map { case (i, j, c) => if (i > j) { (j, i, c) } else { (i, j, c) } }
    new AbstractSymmetricGraph(vertice, newedge, name, linkingEdges, biais)
  }

  implicit def toEdmondsGraph(value: AbstractGraph): Graph = {
    value toEdmondsGraph
  }

  implicit def toHKgraph(value: AbstractGraph): HKgraph = {
    value toHKgraph
  }

}

