/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package graph

/**
 * Timer to ease the gathering of time measurements
 */
object TimeSaver {

  var offset: Long = 0

  def setOffset = {
    offset = System.currentTimeMillis
  }

  def timePassed = {
    System.currentTimeMillis - offset
  }

  def string(time: Long): String = {
    (time / (60 * 1000)) + " min " + ((time % (60 * 1000)) / 1000) + " sec " + (time % 1000) + " ms"
  }
}