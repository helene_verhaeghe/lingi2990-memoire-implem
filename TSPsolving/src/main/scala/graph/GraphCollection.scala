/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package graph

/**
 * Gathering of different graphs for specific tests
 */
object GraphCollection {

  /*
   * multiple cycles, equal value incomming edge
   */
  def graph1: AbstractGraph = {
    AbstractGraph(List(0, 1, 2),
      List((0, 1, 1),
        (1, 0, 10),
        (0, 2, 10),
        (2, 0, 1),
        (1, 2, 10),
        (2, 1, 1)), "graph-1")
  }

  /*
   * graph already arborescence
   */
  def graph2: AbstractGraph = {
    AbstractGraph(List(0, 1, 2),
      List((0, 2, 1),
        (2, 1, 1)), "graph-2")
  }

  /*
   * graph not connected
   */
  def graph3: AbstractGraph = {
    AbstractGraph(List(0, 1, 2, 3),
      List((0, 2, 1),
        (2, 1, 1)), "graph-3")
  }

  /*
   * graph with single cycle
   */
  def graph4: AbstractGraph = {
    AbstractGraph(List(0, 1, 2),
      List((0, 1, 10),
        (2, 1, 1),
        (1, 2, 1)), "graph-4")
  }

  def graph5: AbstractGraph = {
    AbstractGraph(List(0, 1, 2, 3, 4, 5, 6),
      List((0, 1, 1),
        (1, 2, 1),
        (2, 3, 1),
        (3, 4, 1),
        (4, 5, 1),
        (5, 6, 1),
        (4, 2, 1)), "graph-5")
  }

  def graph6: AbstractGraph = {
    AbstractGraph(List(0, 1, 2, 3, 4, 5, 6),
      List((0, 1, 1),
        (1, 2, 2),
        (2, 3, 1),
        (3, 4, 1),
        (4, 5, 1),
        (5, 6, 1),
        (4, 2, 1)), "graph-6")
  }

  // graph du papier
  def graph7: AbstractGraph = {
    AbstractGraph(List(0, 1, 2, 3, 4, 5),
      List((0, 1, 10),
        (0, 2, 2),
        (0, 3, 10),
        (1, 2, 1),
        (2, 3, 4),
        (3, 4, 2),
        (4, 1, 2),
        (3, 5, 4),
        (1, 5, 8)), "graph-7")
  }

  // symmetric graph
  def graph8: AbstractGraph = {
    AbstractGraph(List(0, 1, 2, 3),
      List((0, 1, 1),
        (0, 2, 10),
        (0, 3, 5),
        (1, 2, 10),
        (1, 3, 1),
        (2, 3, 1)), "graph-8",
      true)
  }

  def graph9: AbstractGraph = {
    AbstractGraph(List(0, 1, 2), List((0, 2, 73),
      (1, 0, 88),
      (1, 2, 53),
      (2, 0, 45),
      (2, 1, 53)), "graph-9")

  }

  def graph10: AbstractGraph = {
    AbstractGraph(List(0, 1, 2, 3, 4), List((0, 1, 32),
      (0, 3, 4),
      (0, 4, 50),
      (1, 2, 15),
      (1, 3, 28),
      (1, 4, 28),
      (2, 0, 49),
      (2, 1, 67),
      (2, 4, 30),
      (3, 0, 98),
      (3, 1, 28),
      (3, 2, 58),
      (3, 4, 76),
      (4, 0, 89),
      (4, 1, 99),
      (4, 2, 42),
      (4, 3, 2)), "graph-10")
  }

  // graph ou la borne symmetrique est meilleur que le résultat????
  def graph11: AbstractGraph = {
    AbstractGraph(List(0, 1, 2, 3, 4), List((0, 1, 96),
      (0, 2, 40),
      (0, 3, 73),
      (0, 4, 95),
      (1, 0, 68),
      (1, 2, 99),
      (1, 3, 7),
      (1, 4, 48),
      (2, 0, 50),
      (2, 1, 29),
      (2, 3, 9),
      (2, 4, 32),
      (3, 0, 44),
      (3, 1, 20),
      (3, 2, 67),
      (3, 4, 29),
      (4, 0, 95),
      (4, 1, 94),
      (4, 2, 34),
      (4, 3, 52)), "graph-11")
  }
  //same
  def graph12 = AbstractGraph(List(0, 1, 2, 3, 4), List((0, 1, 41),
    (0, 2, 71),
    (0, 3, 66),
    (0, 4, 72),
    (1, 0, 11),
    (1, 2, 7),
    (1, 3, 3),
    (1, 4, 27),
    (2, 0, 62),
    (2, 1, 16),
    (2, 3, 69),
    (2, 4, 20),
    (3, 0, 85),
    (3, 1, 38),
    (3, 2, 4),
    (3, 4, 78),
    (4, 0, 46),
    (4, 1, 37),
    (4, 2, 11),
    (4, 3, 57)), "graph-12")

  def graph13 = AbstractGraph(List(0, 1, 2, 3), List((0, 1, 81),
    (0, 2, 50),
    (0, 3, 70),
    (1, 0, 63),
    (1, 2, 14),
    (1, 3, 17),
    (2, 0, 83),
    (2, 1, 22),
    (2, 3, 63),
    (3, 1, 22),
    (3, 2, 41)), "graph-13")

  //graph example du Held-karp
  def graph14 = AbstractGraph(List(0, 1, 2, 3), List((0, 1, 10),
    (0, 2, 5),
    (0, 3, 10),
    (1, 2, 5),
    (1, 3, 10),
    (2, 3, 5)), "graph example of Held-Karp (pres)", true)

  // not tight
  def graph15 = AbstractGraph(
    List(0, 1, 2, 3),
    List((0, 1, 4),
      (0, 2, 0),
      (0, 3, 10),
      (1, 0, 17),
      (1, 2, 3),
      (1, 3, 1),
      (2, 0, 10),
      (2, 3, 5),
      (3, 0, 18),
      (3, 1, 10),
      (3, 2, 17)),
    "1426943439003-901 gen-graph (param : n = 4, maxcost = 20, % = 80)",
    false)

}