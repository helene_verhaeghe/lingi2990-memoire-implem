/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package graph

import scala.util.Random

/**
 * Generator of random graphs
 */
object GraphGenerator {

  /**
   * Create graph randomly (graph uniform)
   * nbvertice = nbr of vertices
   * maxcost = maxcost of an edge
   * percent = percent of edge from fullmesh (must be between 0 and 100)
   */
  def gen(idTag: String, nbvertice: Int, maxcost: Int, percent: Int) = {
    val rand = Random
    val vertice = 0 until nbvertice
    val edge =
      for {
        i <- vertice
        j <- vertice
        if (i != j)
        if rand.nextInt(100) < percent
      } yield {
        (i, j, rand.nextInt(maxcost))
      }
    AbstractGraph(vertice, edge, idTag + " gen-graph (param : n = " + nbvertice + ", maxcost = " + maxcost + ", % = " + percent + ")")
  }

  /**
   * Create graph randomly (with high vertex)
   * from 1/5 to 1/3 of the vertex have either
   * all their in either all their out edges
   * with value possibly up to 10 * maxcost
   */
  def genBis(idTag: String, nbvertice: Int, maxcost: Int, percent: Int) = {
    val rand = Random
    val vertice = 0 until nbvertice
    val bb = rand.nextBoolean
    val mix = rand.nextInt(10)
    val per = rand.nextInt(3) + 3
    val edge =
      for {
        i <- vertice
        j <- vertice
        if (i != j)
        if rand.nextInt(100) < percent
      } yield {
        if (bb && i <= nbvertice / per) {
          (i, j, mix * rand.nextInt(maxcost))
        } else if (!bb && j <= nbvertice / per) {
          (i, j, mix * rand.nextInt(maxcost))
        } else {
          (i, j, rand.nextInt(maxcost))
        }

      }
    AbstractGraph(vertice, edge, idTag + " gen-graph (param : n = " + nbvertice + ", maxcost = " + maxcost + ", % = " + percent + ")")
  }
}

/**
 * Store the parameters for further seeding
 */
case class RandomSeed(val name: String,
  val sizeRange: Range,
  val pourcentRange: Range,
  val costRange: Range,
  val nbgraph: Int)

