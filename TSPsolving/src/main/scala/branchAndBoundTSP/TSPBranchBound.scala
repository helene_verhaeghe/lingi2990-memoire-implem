/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package branchAndBoundTSP

import graph._
import scala.collection.mutable.Map

/**
 * Method to solve the TSP by simple branch & bound
 * The graph is given as an instance of AbstractGraph
 */
object TSPBranchBound {
  def apply(graph: AbstractGraph) = {

    TimeSaver.setOffset

    if (graph.isSymmetric) {
      val map = Map() ++ graph.edge.groupBy { case (s, d, c) => s } ++ graph.edge.map { case (s, d, c) => (d, s, c) }.groupBy { case (s, d, c) => s }
      def cond(edgesTaken: Set[List[Int]], i: Int, j: Int): Boolean = { edgesTaken.contains(List(i, j)) || edgesTaken.contains(List(j, i)) }
      resol(graph, map.map(x => (x._1, x._2.toArray)), cond, BranchAndBoundTSP)
    } else {
      val map = Map() ++ graph.edge.groupBy { case (s, d, c) => s }
      def cond(edgesTaken: Set[List[Int]], i: Int, j: Int): Boolean = { edgesTaken.contains(List(i, j)) }
      resol(graph, map.map(x => (x._1, x._2.toArray)), cond, BranchAndBoundATSP)

    }
  }

  def resol(graph: AbstractGraph, map: Map[Int, Array[(Int, Int, Int)]], cond: (Set[List[Int]], Int, Int) => Boolean, implem: Implem) = {
    val nbNode = graph.vertice.size

    var best: List[Int] = List()
    var costbest = Int.MaxValue

    def recur(current: Int, list: List[Int], lvl: Int, cost: Int): Unit = {
      if (cost < costbest && map.contains(current)) {
        if (lvl == nbNode) {
          if (costbest > cost) {
            best = list
            costbest = cost
          }
        } else {
          for {
            (s, d, c) <- map(current)
            if !list.contains(d)
            if (d != 0) || (d == 0 && lvl == nbNode - 1)
          } {
            recur(d, d +: list, lvl + 1, cost + c)
          }
        }
      }
    }
    recur(0, List(), 0, 0)

    if (costbest < Int.MaxValue && !best.isEmpty) {
      val edgesTaken = (0 +: best.reverse).sliding(2).toSet
      val assign: Map[(Int, Int), Option[Int]] = graph.edgeToMapByPairs.map {
        case ((i, j), _) =>
          if (cond(edgesTaken, i, j))
            ((i, j), Some(1))
          else
            ((i, j), Some(0))
      }
      SubGraphTSP(graph, Some(costbest), assign, implem, Some(TimeSaver.timePassed))
    } else {
      SubGraphTSP(graph, None, Map(), implem, Some(TimeSaver.timePassed))
    }
  }

}

object BranchAndBoundATSP extends Implem {
  def asString: String = "Branch & Bound discover of Asymmetric TSP"
}
object BranchAndBoundTSP extends Implem {
  def asString: String = "Branch & Bound discover of Symmetric TSP"
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object TestTSPBranchBound extends App {
  import tsplib.TSPlib
  import printer.GraphvizPrinter

  //  val graph = GraphCollection.graph10
  val graph = TSPlib.br17.toGraph
  val sol = TSPBranchBound(graph)
  println(sol)
  //  val printer = GraphvizPrinter("my")
  //  printer.print(sol)
  //  printer.close
}
