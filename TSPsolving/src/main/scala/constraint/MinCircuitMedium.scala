/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package constraint

import oscar.cp.core.CPIntVar
import oscar.cp.core.CPBoolVar
import heldKarpSymmetric.HeldKarpBound
import oscar.cp.core.CPOutcome
import graph.AbstractGraph
import heldKarpSymmetric.Geometric

class MinCircuitMedium(succ: Array[CPIntVar],
  costTour: CPIntVar,
  edges: Traversable[(Int, Int, Int, CPBoolVar)], //source, dest, cost, var
  name: String = "mincircuitStrong") extends MinCircuit(succ, costTour, edges, name) {

  //  val fct = (i: Int) => new HeldKarpBound(i, succ.size)
  val fct = (i: Int) => new Geometric(100, 0.9)

  def lowerboundCost(ub: Int): Double = {
    val graph = AbstractGraph(0 until succ.length, edges.filter(e => succ(e._1).hasValue(e._2)).map(p => (p._1, p._2, p._3))).getSymmetricVersion.toHKgraph
    val lb = graph.bound(fct(ub), 0, Some(200), (i, j) => Unit, true, ub = ub)
    lb
  }
}