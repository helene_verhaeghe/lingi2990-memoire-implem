/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package constraint

import oscar.cp.modeling._
import oscar.cp.core._
import tsplib.TSPlib
import oscar.algo.reversible.ReversibleInt
import oscar.cp.constraints.AllDifferent
import scala.util.Random

object TSPtest extends CPModel with App {

  val tspgraph = TSPlib.br17
  val absgraph = tspgraph.toGraph

  val cst = MinCircuit(absgraph, CPPropagStrength.Medium)
  add(cst)

  val succ = cst.succ

  minimize(cst.cost)

  val depth = new ReversibleInt(cst.s, 1)
  var pourcent = 0.0
  var bsf = 2000000

  // greedy
  //  search {
  //    println("++")
  //
  //    if (!succ.forall(_.isBound)) {
  //      val (x, v) = cst.nextToBound
  //      branch {
  //        //        println("try assign " + v + " to " + succ.indexOf(x) + " (" + bsf + ")" + "  [" + depth)
  //        depth.setValue(depth.value + 1)
  //        add(x == v)
  //      } {
  //        //        println("rm " + v + " to " + succ.indexOf(x) + " (" + bsf + ")" + " [" + depth)
  //        add(x != v)
  //      }
  //    } else {
  //      noAlternative
  //    }
  //  } onSolution {
  //    bsf = cst.cost.value
  //    println("Solution found :" + cst.succ.map(_.value).mkString(","))
  //  }

  // bff
  search {
    println("++")
    binaryFirstFail(succ, { x =>
      val index = succ.indexOf(x)
      cst.outEdge(index).filter(k => x.hasValue(k._1)).minBy(_._2._1)._1
    })
  } onSolution {
    bsf = cst.cost.value
    println("Solution found :" + cst.succ.map(_.value).mkString(","))
  }

  val stat = start(timeLimit = 5 * 60)
  println(stat)
}