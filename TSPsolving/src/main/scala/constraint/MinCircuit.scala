/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package constraint

import oscar.cp.core._
import oscar.cp.constraints.Circuit
import oscar.cp.constraints.Sum
import edmondsAlgorithms.Graph
import graph.AbstractGraph
import scala.util.Random

/**
 * Constraint modelling the mincircuit
 *   succ : an array of CPIntVar, succ[i] is the successor of i in the tour
 *   costTour : CPIntVar containing the cost of the tour
 *   edges : Traversable of tuple (s,d,c,x)
 *           where s is the id of the source of the edge,
 *           d its destination, c its cost and
 *           x the CPBoolVar selecting it
 */
abstract class MinCircuit(val succ: Array[CPIntVar],
  val cost: CPIntVar,
  val edges: Traversable[(Int, Int, Int, CPBoolVar)], //source, dest, cost, var
  name: String = "mincircuit") extends Constraint(cost.store, name) {

  val matrixCosts = Array.fill(succ.length)(Array.fill(succ.length)(Int.MaxValue))
  for { (s, d, c, x) <- edges } {
    matrixCosts(s)(d) = c
  }

  /**
   * Maps to access easely the in and out edge of a vertice
   */
  val inEdge = edges.groupBy(_._2).map { case (id, t) => (id, t.map { case (s, d, c, x) => (s, (c, x)) }.toMap) }
  val outEdge = edges.groupBy(_._1).map { case (id, t) => (id, t.map { case (s, d, c, x) => (d, (c, x)) }.toMap) }

  /**
   * Return a tuple (x,v) where x is a still
   * unbound variable and v a value of its domain
   * such that the cost of the edge from x to v
   * is the smallest available
   */
  def nextToBound = {
    val p1 = succ.zipWithIndex.filter(!_._1.isBound)
    val pmin = p1.minBy(t => t._1.size)._1.size
    val p2 = p1.filter(t => t._1.size == pmin)
    val p3 = p2.map {
      t =>
        val mm = outEdge(t._2).filter(!_._2._2.isBound).minBy(k => k._2._1)
        (t._1, t._2, mm._1, mm._2._1)
    }.minBy(_._4)
    (p3._1, p3._3)
  }

  def costValue = {
    var sum = 0
    for (i <- 0 until succ.length) {
      sum += outEdge(i)(succ(i).value)._1
    }
    sum
  }
  /**
   * Circuit constraint over the succ array
   */
  val circuit = new Circuit(succ)

  /**
   * Sum constraint linking the edge selection to the variable of the cost
   */
  val sum = new Sum(edges.map { case (s, d, c, x) => x * c }.toArray, cost)

  /**
   * Compute the mean of the variance of the edges cost at each node defined by the map
   */
  private def meanvariance(map: Map[Int, Map[Int, (Int, CPIntVar)]]): Double = {
    var sum = 0.0
    for (i <- 0 until succ.length) {
      var sumnode = 0.0
      var num = 0
      for {
        (id, (c, x)) <- map(i)
        if !x.isBoundTo(0)
      } {
        sumnode += c
        num += 1
      }
      val mean = 1.0 * sumnode / num
      sumnode = 0
      for {
        (id, (c, x)) <- map(i)
        if !x.isBoundTo(0)
      } {
        sumnode += (c - mean) * (c - mean)
      }
      sum += 1.0 * sumnode / num
    }
    sum / succ.length
  }

  /**
   * Boolean test to predict if the use of the Arborescence
   * is more likely to result in a better lower bound
   */
  def betterUseArbo: Boolean = {
    val inVar = this.meanvariance(inEdge)
    val outVar = this.meanvariance(outEdge)
    if (outVar == inVar)
      Random.nextBoolean
    else
      outVar >= inVar
  }

  /**
   * Compute an upper bound of the maximum cost of the tour
   */
  def ubmaxcost: Int = {
    var sum = 0
    for (s <- 0 until succ.length) {
      if (succ(s).isBound) {
        sum += outEdge(s)(succ(s).value)._1
      } else {
        var maxvalue = 0
        for {
          (d, (c, x)) <- outEdge(s)
          if (!x.isBoundTo(0))
        } {
          maxvalue = Math.max(maxvalue, c)
        }
        sum += maxvalue
      }
    }
    sum
  }

  /**
   * Setup method
   */
  def setup(l: CPPropagStrength): CPOutcome = {
    if (s.add(circuit) == CPOutcome.Failure)
      return CPOutcome.Failure
    if (s.add(sum) == CPOutcome.Failure)
      return CPOutcome.Failure

    for {
      sour <- 0 until succ.length
      dest <- 0 until succ.length
    } {
      if (outEdge.contains(sour) && outEdge(sour).contains(dest)) {
        // edge between s and d
        val (c, x) = outEdge(sour)(dest)
        if (s.add((x == (succ(sour) === dest))) == CPOutcome.Failure)
          return CPOutcome.Failure
      } else {
        // no edge between s and d
        if (s.add(succ(sour) != dest) == CPOutcome.Failure)
          return CPOutcome.Failure
      }
    }
    for (i <- 0 until succ.length) {
      succ(i).callPropagateWhenDomainChanges(this)
    }
    propagate()
  }

  override def propagate(): CPOutcome = {
    if (!inEdge.forall { i => !i._2.filter(j => succ(j._1).hasValue(i._1)).isEmpty })
      return CPOutcome.Failure
    val ub = outEdge.map { i => (i._1, i._2.filter(j => succ(i._1).hasValue(j._1)).maxBy(_._2._1)) }.map(_._2._2._1).sum
    if (cost.updateMax(ub) == CPOutcome.Failure)
      return CPOutcome.Failure
    val lb = this.lowerboundCost(ub)
    if (cost.updateMin(lb.toInt + 1) == CPOutcome.Failure) {
      CPOutcome.Failure
    } else {
      //      println("lb : " + cost.min + " ub : " + cost.max)
      CPOutcome.Suspend
    }
  }

  def lowerboundCost(ub: Int): Double
}

object MinCircuit {
  def apply(succ: Array[CPIntVar], costTour: CPIntVar, edges: Traversable[(Int, Int, Int, CPBoolVar)], l: CPPropagStrength): MinCircuit = {
    if (l == CPPropagStrength.Strong) {
      new MinCircuitStrong(succ, costTour, edges)
    } else if (l == CPPropagStrength.Medium) {
      new MinCircuitMedium(succ, costTour, edges)
    } else {
      new MinCircuitWeak(succ, costTour, edges)
    }
  }
  def apply(graph: AbstractGraph, l: CPPropagStrength = CPPropagStrength.Strong)(implicit store: CPStore): MinCircuit = {
    val listv = graph.listVertice
    val succ = Array.tabulate(listv.size) { i => CPIntVar(store, 0 until listv.size) }
    val li = graph.edge.map { x => (x._1, x._2, x._3, CPBoolVar()) }
    val cost = CPIntVar(store, 0 to Int.MaxValue - 1)
    MinCircuit(succ, cost, li, l)
  }
}