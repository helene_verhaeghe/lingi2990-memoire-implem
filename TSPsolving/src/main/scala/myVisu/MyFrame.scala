/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package myVisu

import oscar.visual._
import org.jfree.chart.title._
import org.jfree.chart._

/**
 * Extension of the OscaR visual package to ease
 * the use of plotline graph for the results
 *
 * OscaR : http://oscarlib.bitbucket.org/
 */
class MyFrame {

  val frame = VisualFrame("Results")

  def addPlotLine(title: String, xlab: String, ylab: String, nbSeries: Int = 1, labelLegend: Traversable[String] = Traversable()) = {
    val inf = frame.createFrame(title)
    val plotLine = MyPlotLine(title, xlab, ylab, nbSeries, labelLegend)
    inf.add(plotLine)
    inf.pack()
    plotLine
  }
}

object MyFrame {
  def apply() = new MyFrame()
}