/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package myVisu

import oscar.visual.plot.PlotLine
import org.jfree.chart.plot._
import org.jfree.chart._
import java.awt.Color

/**
 * Extension of the OscaR visual package to ease
 * the use of plotline graph for the results
 *
 * OscaR : http://oscarlib.bitbucket.org/
 */
class MyPlotLine(title: String, xlab: String, ylab: String, nbSeries: Int = 1) extends PlotLine(title, xlab, ylab, nbSeries) {

  override def createChart = ChartFactory.createXYLineChart(title, xlab, ylab, xyDataset, PlotOrientation.VERTICAL, true, false, false);

  def addTo(serie: Int): (Int, Double) => Unit = (x, y) => this.addPoint(x, y, serie)
}

object MyPlotLine {
  def apply(title: String, xlab: String, ylab: String, nbSeries: Int = 1, labelLegend: Traversable[String] = Traversable()) = {
    val graphPlot = new MyPlotLine(title, xlab, ylab, nbSeries)
    val colorGen = ColorGenerator(nbSeries)
    for {
      serie <- 0 until nbSeries
    } {
      graphPlot.plot.getRenderer().setSeriesPaint(serie, ColorGenerator(serie))
    }

    if (nbSeries == labelLegend.size) {
      val legendItems = graphPlot.plot.getLegendItems
      val legcoll = new LegendItemCollection()
      val iter = legendItems.iterator()
      for (newlabel <- labelLegend) {
        val legend = iter.next.asInstanceOf[LegendItem]
        legcoll.add(new LegendItem(newlabel, legend.getDescription, legend.getToolTipText(), legend.getURLText(), legend.getLine(), legend.getLineStroke(), legend.getFillPaint()))
      }
      graphPlot.plot.setFixedLegendItems(legcoll)
    }
    graphPlot
  }
}

/**
 * Class to generate up to 90 different colors for graphs
 * They are generated to avoid too light colors not well saw
 * on white charts
 */
object ColorGenerator {

  val col: Array[Float] = Array(0.0f, 240.0f, 130.0f, 300.0f, 30.0f, 180.0f, 270.0f, 80.0f, 210.0f).map(x => x / 360.0f)
  val sat: Array[Float] = Array(0.7f, 0.4f)
  val ligh: Array[Float] = Array(1.0f, 0.9f, 0.8f, 0.7f, 0.6f)

  private def genTrip(nbserie: Int): (Int, Int, Int) = {
    (nbserie % col.size, (nbserie / col.size) % sat.size, nbserie / (col.size * sat.size) % ligh.size)
  }

  /**
   * Generate a color
   * nbserie should be between 0 and 89
   */
  def apply(nbserie: Int): Color = {
    val trip = genTrip(nbserie)
    Color.getHSBColor(col(trip._1), sat(trip._2), ligh(trip._3))
  }

}