/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package myVisu

import oscar.visual._
import java.awt.Color

/**
 * Class to gather multiple lines and merge them into a MyPlotline
 *
 * Based on the OscaR visual package
 *
 * OscaR : http://oscarlib.bitbucket.org/
 */
class ResultGraph(val frame: VisualFrame, val title: String, val xlab: String, val ylab: String, val xmin: Double, val xmax: Double) {

  var listLine: List[ResultLines] = List() // listed from the last to the first
  var plotline: Option[MyPlotLine] = None
  var serie = 0

  private def nbSeries: Int = serie

  private def labelLegend: Traversable[String] = {
    val res = Array.fill(nbSeries)("")
    var i = nbSeries - 1
    for {
      line <- listLine
      name <- line.getName
    } {
      res(i) = name
      i -= 1
    }
    res
  }

  def isGenerated = !plotline.isEmpty

  def addTo(serie: Int, x: Double, y: Double) = {
    if (isGenerated) {
      plotline.get.addPoint(x, y, serie)
    }
  }

  def getAddTo(serie: Int): (Double, Double) => Unit = {
    val self = this
    (x, y) => self.addTo(serie, x, y)
  }

  def create = {
    val inf = frame.createFrame(title)
    val plotLine = MyPlotLine(title, xlab, ylab, nbSeries, labelLegend)
    inf.add(plotLine)
    inf.pack()
    plotLine.xDom = xmin.toInt to (xmax.toInt + 1)
    plotline = Some(plotLine)
    for (line <- listLine) { line.preGen(xmin, xmax) }
  }

  private def addLine(line: ResultLines): ResultLines = {
    serie += line.getNumbers
    listLine = line +: listLine
    line
  }
  private def checkNotGenerated = {
    if (isGenerated) { throw new Exception("Graph already generated") }
  }
  private def checkGenerated = {
    if (!isGenerated) { throw new Exception("Graph not yet generated") }
  }

  def getHorizontalLine(name: String, value: Double) = {
    checkNotGenerated
    addLine(HorizontalLine(name, value, getAddTo(serie)))
  }

  def getVerticalLine(name: String, value: Double, ymin: Double, ymax: Double) = {
    checkNotGenerated
    addLine(VerticalLine(name, value, ymin, ymax, getAddTo(serie)))
  }

  def getPerformanceProfileLine(name: String) = {
    checkNotGenerated
    addLine(PerformanceProfileLine(name, getAddTo(serie)))
  }

  def getEvolutiveLine(name: String) = {
    checkNotGenerated
    addLine(EvolutiveLine(name, getAddTo(serie)))
  }

  def getEvolutiveWithMax(name: String) = {
    checkNotGenerated
    addLine(EvolutiveLineWithMax(name, getAddTo(serie), getAddTo(serie + 1)))
  }

  def getMaxOfEvolutive(name: String) = {
    checkNotGenerated
    addLine(MaxOfEvolutiveLine(name, getAddTo(serie)))
  }

  def printInfo = {
    checkGenerated
    val plot = plotline.get.chart
    val xyplot = plot.getXYPlot()
    println(plot.getTitle.getText()) // titre du graph
    println(xyplot.getDataset().getSeriesCount()) // nombre de série
    println(xyplot.getLegendItems().get(42).getLabel()) // label de la série
    println(xyplot.getDataset.getItemCount(42))
    println(xyplot.getDataset.getXValue(42, 0))
    println(xyplot.getDataset.getYValue(42, 0))
    println(xyplot.getDataset.getXValue(42, 1))
    println(xyplot.getDataset.getYValue(42, 1))

  }

}

/**
 * Companion object
 */
object ResultGraph {
  def apply(title: String, xlab: String, ylab: String, xmin: Double, xmax: Double): ResultGraph = {
    new ResultGraph(VisualFrame("Results"), title, xlab, ylab, xmin, xmax)
  }
  def apply(frame: VisualFrame, title: String, xlab: String, ylab: String, xmin: Double, xmax: Double): ResultGraph = {
    new ResultGraph(frame, title, xlab, ylab, xmin, xmax)
  }
}

/**
 * Different type of line to set on a ResultGraph
 */
trait ResultLines {
  val name: String
  val addTo: (Double, Double) => Unit

  def getNumbers = 1

  def getName = List(name)

  def addPoint(x: Double, y: Double) = {
    addTo(x, y)
  }

  def addPoints(list: Traversable[(Double, Double)]) = {
    for ((x, y) <- list) {
      this.addPoint(x, y)
    }
  }

  def preGen(xmin: Double, xmax: Double): Unit = {
  }
}

/**
 * Create a line with equation y=ax+b
 */
case class LinearLine(val name: String, val a: Double, val b: Double, val addTo: (Double, Double) => Unit) extends ResultLines {
  override def preGen(xmin: Double, xmax: Double): Unit = {
    super.preGen(xmin, xmax)
    this.addTo(xmin, (a * xmin) + b)
    this.addTo(xmax, (a * xmin) + b)
  }
  override def addPoint(x: Double, y: Double) = {}

}

/**
 * Create a horizontal line with equation y=value
 */
class HorizontalLine(name: String, val value: Double, addTo: (Double, Double) => Unit) extends LinearLine(name, 0, value, addTo)
object HorizontalLine {
  def apply(name: String, value: Double, addTo: (Double, Double) => Unit) = new HorizontalLine(name, value, addTo)
}

/**
 * Create a vertical line with equation x=value
 */
case class VerticalLine(name: String, val value: Double, val ymin: Double, val ymax: Double, addTo: (Double, Double) => Unit) extends ResultLines {
  override def preGen(xmin: Double, xmax: Double): Unit = {
    super.preGen(xmin, xmax)
    this.addTo(value, ymin)
    this.addTo(value, ymax)
  }
  override def addPoint(x: Double, y: Double) = {}
}

/**
 * Create a perforance profile line
 */
case class PerformanceProfileLine(val name: String, val addTo: (Double, Double) => Unit) extends ResultLines {
  var previousValue: Option[Double] = None
  override def addPoint(x: Double, y: Double) = {
    if (!previousValue.isEmpty) {
      addTo(x, previousValue.get)
    }
    previousValue = Some(y)
    super.addPoint(x, y)
  }

}

/**
 * Create a simple evolutive line
 */
case class EvolutiveLine(
  val name: String,
  val addTo: (Double, Double) => Unit) extends ResultLines

/**
 * Create a double line : one is an evolutive,
 * the other keep track of the maximum
 */
case class EvolutiveLineWithMax(
  val name: String,
  addToPoint: (Double, Double) => Unit,
  addToMax: (Double, Double) => Unit)
  extends ResultLines {

  var currentMax = Double.MinValue
  val addTo: (Double, Double) => Unit = (x, y) => {
    if (y >= currentMax) { currentMax = y }
    addToPoint(x, y)
    addToMax(x, currentMax)
  }

  override def getName = List("Maximum of " + name, name)
  override def getNumbers = 2
}

/**
 * Create a line keeping only track of the
 * maximum of the points feeded
 */
case class MaxOfEvolutiveLine(
  val name: String,
  addToPoint: (Double, Double) => Unit)
  extends ResultLines {

  var currentMax = Double.MinValue
  val addTo: (Double, Double) => Unit = (x, y) => {
    if (y >= currentMax) { currentMax = y }
    addToPoint(x, currentMax)
  }

  override def getName = List("Maximum of " + name)

}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object TestResultGraph extends App {
  val graph = ResultGraph("frame title", "iteration", "hight", 0, 10)
  graph.getHorizontalLine("line 5", 5)
  val evol = graph.getEvolutiveLine("evol\\%")
  val evol2 = graph.getEvolutiveWithMax("evol max")
  graph.create
  evol.addPoint(2, 4)
  evol.addPoint(6, 3)
  evol.addPoint(7, 6)
  evol.addPoint(9, 2)
  evol2.addPoint(1, 4)
  evol2.addPoint(2, 3)
  evol2.addPoint(4, 6)
  evol2.addPoint(10, 2)

  import printer._
  TikzGraphicPrinter("testtikz", graph)
}

object TestPalette extends App {
  val graph = ResultGraph("frame title", "iteration", "hight", 0, 10)
  for (i <- 0 until 90) {
    graph.getHorizontalLine("" + i, i)
  }
  graph.create

  graph.printInfo
}
