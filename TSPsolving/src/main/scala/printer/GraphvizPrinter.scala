/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package printer

import java.io.PrintWriter
import graph._

/**
 * Class to print graph following the Graphviz notation
 *
 * Graphviz : http://www.graphviz.org/
 */
class GraphvizPrinter(val name: String) extends Printer {

  val printwriter: PrintWriter = new PrintWriter(name + ".gv")
  var isClosed = false

  def print(graph: AbstractSymmetricGraph) = {
    if (!isClosed) {
      printwriter.println("graph \"" + graph.name + "\" {")
      for ((s, d, c) <- graph.edge) {
        printwriter.println("\t" + s + " -> " + d + " [label=" + c + "]")
      }
      printwriter.println("}")
    }
  }

  def print(graph: AbstractGraph) = {
    if (!isClosed) {
      printwriter.println("digraph \"" + graph.name + "\" {")
      for ((s, d, c) <- graph.edge) {
        printwriter.println("\t" + s + " -> " + d + " [label=" + c + "]")
      }
      printwriter.println("}")
    }
  }

  def print(graph: SubGraphTSP) = {
    if (!isClosed) {
      printwriter.println("digraph \"Solution of " + graph.from + " of graph" + graph.graph.name + "\" {")
      for {
        (s, d, c) <- graph.graph.edge
        if graph.assign.contains((s, d))
        if !graph.assign((s, d)).isEmpty
        if graph.assign((s, d)) == Some(1)
      } {
        printwriter.println("\t" + s + " -> " + d + " [label=" + c + "]")
      }
      printwriter.println("}")
    }
  }
}

object GraphvizPrinter {
  def apply(name: String) = new GraphvizPrinter(name)
}

/**
 *  ===========
 *  || TESTS ||
 *  ===========
 */

object TestGraphvizPrinter extends App {
  import tsplib._
  for (tsplibgraph <- TSPlib.ALL_TSPlib) {
    val graph = tsplibgraph.toGraph
    val printer = GraphvizPrinter("output/graphprint/" + graph.name)
    printer.print(graph)
    printer.close
  }
}