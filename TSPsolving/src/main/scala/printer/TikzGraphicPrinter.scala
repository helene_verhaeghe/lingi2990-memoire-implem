/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package printer

import java.io.PrintWriter
import myVisu.ResultGraph
import myVisu.ColorGenerator

/**
 * Print corresponding tikz representation of the ResultGraph
 */
object TikzGraphicPrinter {
  def apply(name: String, graph: ResultGraph, threshold: Double = 0.001) = {
    println("PrintTikz : Start")
    val p: PrintWriter = new PrintWriter("output/tikz/" + name + ".tex")

    println("PrintTikz : Get plot info")
    val plot = graph.plotline.get.chart
    val xyplot = plot.getXYPlot()
    val data = xyplot.getDataset()
    val legend = xyplot.getLegendItems()

    p.println("\\begin{tikzpicture}")

    println("PrintTikz : Print colors")
    p.println("")
    for (serie <- 0 until data.getSeriesCount()) {
      val color = ColorGenerator(serie)
      p.println("\t\\definecolor{C" + serie + "}{RGB}{" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + "}")
    }
    p.println("")

    println("PrintTikz : Print axis")
    p.println("\t\\begin{axis}[xlabel=$" + graph.xlab + "$,ylabel=$" + graph.ylab + "$,legend style={at={(1,1)},anchor=north west,at={(axis description cs:1.05,1)}}" +
      ",\nxmin = " + graph.xmin + ",\nxmax = " + graph.xmax + ",\nwidth=0.8\\linewidth,\nheight=6cm]")

    for (serie <- 0 until data.getSeriesCount()) {
      println("PrintTikz : Print serie " + (serie + 1) + "/" + data.getSeriesCount())
      p.println("\t\\addplot[mark=\"\",C" + serie + "] plot coordinates {")
      var savea = (data.getXValue(serie, 0), data.getYValue(serie, 0))
      var saveb = (data.getXValue(serie, 0), data.getYValue(serie, 0))
      for (id <- 1 until data.getItemCount(serie)) {
        val savec = (data.getXValue(serie, id), data.getYValue(serie, id))
        if (Math.abs(savec._1 - saveb._1) >= threshold) {
          p.println("\t(" + savea._1 + "," + savea._2 + ")")
          if (Math.abs(savea._2 - saveb._2) >= threshold) {
            p.println("\t(" + saveb._1 + "," + saveb._2 + ")")
          }
          savea = savec
        }
        saveb = savec
      }
      p.println("\t(" + savea._1 + "," + savea._2 + ")")
      if (Math.abs(savea._2 - saveb._2) >= threshold) {
        p.println("\t(" + saveb._1 + "," + saveb._2 + ")")
      }
      p.println("\t};")
      p.println("\t\\addlegendentry{" + legend.get(serie).getLabel() + "}")
    }

    p.println("\t\\end{axis}")
    p.println("\\end{tikzpicture}")
    p.close
    println("PrintTikz : End")
  }

}
