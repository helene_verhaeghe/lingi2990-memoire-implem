/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package printer

import java.io.PrintWriter
import graph._
import comparison.AllTestingResultTSP

/**
 * Class to print in a file the result of the
 * comparison between the LP
 */
class AllTestingResultTSPPrinter(
  val name: String,
  val printCond: AllTestingResultTSP => Boolean = _ => true) extends Printer {

  val printwriter: PrintWriter = new PrintWriter(name + ".txt")
  var isClosed = false

  /**
   * Print a short version of the result
   */
  def feed(index: Int, result: AllTestingResultTSP): Boolean = {
    this.feeding(index, result, _.toString)
  }

  /**
   * Print complete version of the result
   */
  def feedComplete(index: Int, result: AllTestingResultTSP): Boolean = {
    this.feeding(index, result, _.toStringComplete)
  }

  private def feeding(index: Int, result: AllTestingResultTSP, printmethod: AllTestingResultTSP => String): Boolean = {
    if (!isClosed && printCond(result)) {
      printwriter.println(index)
      printwriter.println(printmethod(result))
      true
    } else {
      false
    }
  }
}
/**
 * Companion object
 */
object ResultPrinter {
  def apply(name: String, printCond: AllTestingResultTSP => Boolean = _ => true) = new AllTestingResultTSPPrinter(name, printCond)
}

