/**
 * Code written for the fulfillment of the Thesis submitted
 * for the Master's degree in computer science and engineering
 * Ecole Polytechnique de Louvain
 * Universite Catholique de Louvain
 * Belgium
 *
 * Copyright: Verhaeghe Helene
 * <helene.verhaeghe27@gmail.com>
 * 2014-2015
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package runner;

import comparison.ComparisonIterRandA;
import comparison.ComparisonIterRandB;
import comparison.ComparisonIterRandC;
import comparison.ComparisonIterTSPlib;
import comparison.ComparisonRandom;
import comparison.ComparisonStart;
import comparison.ComparisonStartRandB;
import comparison.ComparisonStartRandC;
import comparison.ComparisonStartTSPlib;
import comparison.ComparisonTSPLib;

/**
 * Java class used to launch method when using a jar
 */
public class Runner {
	public static void main(String args[]) {

		if (args.length == 0) {
			System.out.println("");
		} else {
			if (args[0].equals("1"))
				System.out.println("test");
			else if (args[0].equals("--tsplib"))
				ComparisonTSPLib.main(null);
			else if (args[0].equals("--random"))
				ComparisonRandom.main(null);
			else if (args[0].equals("--compStartTSPlib"))
				ComparisonStartTSPlib.main(null);
			else if (args[0].equals("--compStartRandA"))
				ComparisonStart.main(null);
			else if (args[0].equals("--compStartRandB"))
				ComparisonStartRandB.main(null);
			else if (args[0].equals("--compStartRandC"))
				ComparisonStartRandC.main(null);
			else if (args[0].equals("--compIterRandA"))
				ComparisonIterRandA.main(null);
			else if (args[0].equals("--compIterRandB"))
				ComparisonIterRandB.main(null);
			else if (args[0].equals("--compIterRandC"))
				ComparisonIterRandC.main(null);
			else if (args[0].equals("--compIterTSPlib"))
				ComparisonIterTSPlib.main(null);
			else if (args[0].equals("--help") || args[0].equals("-h")) {
				System.out
						.println("To launch tests use one of the following option");
				System.out.println("\t--compStartTSPlib");
				System.out.println("\t--compStartRandA");
				System.out.println("\t--compStartRandB");
				System.out.println("\t--compStartRandC");
				System.out.println("\t--compIterTSPlib");
				System.out.println("\t--compIterRandA");
				System.out.println("\t--compIterRandB");
				System.out.println("\t--compIterRandC");
			}
		}

	}
}
